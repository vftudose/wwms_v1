<?php

namespace App\Security\Auth0ManagementClient\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class CreateUser
{
    const CONNECTION_TYPE_USERNAME_PASSWORD = 'Username-Password-Authentication';

    /**
     * @Assert\NotBlank
     */
    public $name;

    /**
     * @Assert\Email
     */
    public $email;

    /**
     * @Assert\NotBlank
     */
    public $password;

    public function serialize()
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
            'connection' => self::CONNECTION_TYPE_USERNAME_PASSWORD
        ];
    }
}