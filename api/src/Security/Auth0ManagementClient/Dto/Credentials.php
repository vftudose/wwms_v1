<?php

namespace App\Security\Auth0ManagementClient\Dto;

class Credentials
{
    public $domain;
    public $audience;
    public $clientId;
    public $clientSecret;

    public function __construct($domain, $audience, $clientId, $clientSecret)
    {
        $this->domain = $domain;
        $this->audience = $audience;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    public function serialize(): array
    {
        $data = [];

        if ($this->domain !== null) {
            $data['domain'] = $this->domain;
        }

        if ($this->audience !== null) {
            $data['audience'] = $this->audience;
        }

        if ($this->clientId !== null) {
            $data['client_id'] = $this->clientId;
        }

        if ($this->clientSecret !== null) {
            $data['client_secret'] = $this->clientSecret;
        }

        return $data;
    }
}