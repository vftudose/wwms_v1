<?php

namespace App\Security\Auth0ManagementClient\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class UpdateUser
{
    const CONNECTION_TYPE_USERNAME_PASSWORD = 'Username-Password-Authentication';

    /**
     * @Assert\NotBlank
     */
    public $id;

    public $name;

    /**
     * @Assert\Email
     */
    public $email;

    public $password;

    public function serialize()
    {
        $data = [
            'connection' => self::CONNECTION_TYPE_USERNAME_PASSWORD
        ];

        if ($this->name !== null) {
            $data['name'] = $this->name;
        }

        if ($this->email !== null) {
            $data['email'] = $this->email;
        }

        if ($this->password !== null) {
            $data['password'] = $this->password;
        }

        return $data;
    }
}