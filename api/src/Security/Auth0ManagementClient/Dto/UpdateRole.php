<?php

namespace App\Security\Auth0ManagementClient\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class UpdateRole
{
    /**
     * @Assert\NotBlank
     */
    public $id;

    public $name;

    public $description;

    public function serialize()
    {
        return [
            'name' => $this->name,
            'description' => $this->description
        ];
    }
}