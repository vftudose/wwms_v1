<?php

namespace App\Security\Auth0ManagementClient\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class CreateRole
{
    /**
     * @Assert\NotBlank
     */
    public $name;

    /**
     * @Assert\NotBlank
     */
    public $description;

    public function serialize()
    {
        return [
            'name' => $this->name,
            'description' => $this->description
        ];
    }
}