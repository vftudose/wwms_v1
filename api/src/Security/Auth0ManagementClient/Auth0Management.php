<?php

namespace App\Security\Auth0ManagementClient;

use App\Security\Auth0ManagementClient\Dto\CreateRole;
use App\Security\Auth0ManagementClient\Dto\Credentials;
use App\Security\Auth0ManagementClient\Dto\CreateUser;
use App\Security\Auth0ManagementClient\Dto\UpdateRole;
use App\Security\Auth0ManagementClient\Dto\UpdateUser;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class Auth0Management
{
    const AUTH_ENDPOINT = '/oauth/token';
    const USERS_ENDPOINT = '/api/v2/users';
    const ROLES_ENDPOINT = '/api/v2/roles';

    private $guzzle;

    public function __construct()
    {
        $this->guzzle =  new Client();
    }

    public function authenticate(Credentials $credentials)
    {
        $uri = new Uri("https://". $credentials->domain . self::AUTH_ENDPOINT);

        return $this->guzzle->post($uri, [
            RequestOptions::FORM_PARAMS => [
                'grant_type' => 'client_credentials',
                'audience' => $credentials->audience,
                'client_id' => $credentials->clientId,
                'client_secret' => $credentials->clientSecret
            ]
        ]);
    }

    public function getUserRoles(string $domain, string $accessToken, string $userId)
    {
        $uri = new Uri("https://" . $domain . self::USERS_ENDPOINT . "/{$userId}/roles");

        return $this->guzzle->get($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ]);
    }

    public function createUser(string $domain, string $accessToken, CreateUser $user)
    {
        $uri = new Uri("https://" . $domain . self::USERS_ENDPOINT);

        return $this->guzzle->post($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            RequestOptions::FORM_PARAMS => $user->serialize()
        ]);
    }

    public function getUser(string $domain, string $accessToken, string $userId)
    {
        $uri = new Uri("https://" . $domain . self::USERS_ENDPOINT . '/' . $userId );

        return $this->guzzle->get($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ]);
    }

    public function updateUser(string $domain, string $accessToken, UpdateUser $user)
    {
        $uri = new Uri("https://" . $domain . self::USERS_ENDPOINT . '/' . $user->id);

        return $this->guzzle->patch($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            RequestOptions::FORM_PARAMS => $user->serialize()
        ]);
    }

    public function deleteUser(string $domain, string $accessToken, string $userId)
    {
        $uri = new Uri("https://" . $domain . self::USERS_ENDPOINT . '/' . $userId);

        return $this->guzzle->delete($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ]);
    }

    public function getAllUsers(string $domain, string $accessToken)
    {
        $uri = new Uri("https://" . $domain . self::USERS_ENDPOINT);

        return $this->guzzle->get($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ]);
    }

    public function assignUserRoles(string $domain, string $accessToken, string $userId, array $roles)
    {
        $uri = new Uri("https://" . $domain . self::USERS_ENDPOINT . '/' . $userId . '/roles');

        return $this->guzzle->post($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            RequestOptions::FORM_PARAMS => [
                'roles' => $roles
            ]
        ]);
    }

    public function removeRolesFromUser(string $domain, string $accessToken, string $userId, array $roles)
    {
        $uri = new Uri("https://" . $domain . self::USERS_ENDPOINT . '/' . $userId . '/roles');

        return $this->guzzle->delete($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            RequestOptions::FORM_PARAMS => [
                'roles' => $roles
            ]
        ]);
    }

    public function getAllRoles(string $domain, string $accessToken)
    {
        $uri = new Uri("https://" . $domain . self::ROLES_ENDPOINT);

        return $this->guzzle->get($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ]);
    }

    public function createRole(string $domain, string $accessToken, CreateRole $role)
    {
        $uri = new Uri("https://" . $domain . self::ROLES_ENDPOINT);

        return $this->guzzle->post($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            RequestOptions::FORM_PARAMS => $role->serialize()
        ]);
    }

    public function getRole(string $domain, $accessToken, $roleId)
    {
        $uri = new Uri("https://" . $domain . self::ROLES_ENDPOINT . '/' . $roleId );

        return $this->guzzle->get($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ]
        ]);
    }

    public function updateRole(string $domain, string $accessToken, UpdateRole $role)
    {
        $uri = new Uri("https://" . $domain . self::ROLES_ENDPOINT . '/' . $role->id);

        return $this->guzzle->patch($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
            RequestOptions::FORM_PARAMS => $role->serialize()
        ]);
    }

    public function deleteRole(string $domain, string $accessToken, string $roleId)
    {
        $uri = new Uri("https://" . $domain . self::ROLES_ENDPOINT . '/' . $roleId);

        return $this->guzzle->delete($uri, [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . $accessToken
            ],
        ]);
    }

    public function getAccessToken()
    {
        $cache = RedisAdapter::createConnection($_ENV['REDIS_CACHE_PROVIDER']);

        $accessTokenCacheKey = md5('management_access_token');

        if ($cache->exists($accessTokenCacheKey)) {
            $token = $cache->get($accessTokenCacheKey);
        } else {
            $response = $this->authenticate(new Credentials(
                $_ENV['AUTH0_DOMAIN'],
                $_ENV['AUTH0_MANAGEMENT_AUDIENCE'],
                $_ENV['AUTH0_CLIENT_ID'],
                $_ENV['AUTH0_CLIENT_SECRET']
            ));

            $auth = json_decode($response->getBody()->getContents());
            $token = $auth->access_token;
            $cache->setex($accessTokenCacheKey, $auth->expires_in, $token);
        }

        return $token;
    }
}