<?php

namespace App\Security;

use App\Security\Auth0ManagementClient\Auth0Management;
use App\Security\Auth0ManagementClient\Dto\Credentials;
use Auth0\JWTAuthBundle\Security\Auth0Service;
use Auth0\JWTAuthBundle\Security\Core\JWTUserProviderInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Symfony\Component\Security\Core\User\UserInterface;

class A0UserProvider implements JWTUserProviderInterface
{
    protected $auth0Service;
    protected $auth0Management;

    public function __construct(Auth0Service $auth0Service, Auth0Management $auth0Management)
    {
        $this->auth0Service = $auth0Service;
        $this->auth0Management = $auth0Management;
    }

    public function loadUserByJWT($jwt)
    {
        $cache = RedisAdapter::createConnection($_ENV['REDIS_CACHE_PROVIDER']);
        $rolesCacheKey = md5($jwt->sub . '_' . 'roles');

        if ($cache->exists($rolesCacheKey)) {
            $roles = json_decode($cache->get($rolesCacheKey), true);
        } else {
            $token = $this->auth0Management->getAccessToken();

            $response = $this->auth0Management->getUserRoles(
                $_ENV['AUTH0_DOMAIN'],
                $token,
                $jwt->sub
            );

            $roles = array_column(json_decode($response->getBody()->getContents(), true), 'name');

            $cache->setex($rolesCacheKey, 86000, json_encode($roles));
        }

        return new A0User($jwt, $roles);
    }

    public function loadUserByUsername($username)
    {
        throw new NotImplementedException('method not implemented');
    }

    public function getAnonymousUser()
    {
        return new A0AnonymousUser();
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof WebserviceUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'AppBundle\Security\A0User';
    }
}