<?php

namespace App\Controller;

use App\Security\Auth0ManagementClient\Auth0Management;
use App\Security\Auth0ManagementClient\Dto\CreateUser;
use App\Security\Auth0ManagementClient\Dto\UpdateUser;
use Craft\Utility\ErrorBeautifier;
use GuzzleHttp\Exception\ClientException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UsersController extends AbstractController
{
    private $validator;
    private $auth0Management;

    public function __construct(
        ValidatorInterface $validator,
        Auth0Management $auth0Management
    ) {
        $this->validator = $validator;
        $this->auth0Management = $auth0Management;
    }

    /**
     * @Route("api/v1/users/{user_id}", name="get_user", methods={"GET"})
     * @param Request $request
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        if (!$request->get('user_id')) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'user id is required'
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->getUser($_ENV['AUTH0_DOMAIN'], $accessToken, $request->get('user_id'));
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/users", name="get_all_user", methods={"GET"})
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @return JsonResponse
     */
    public function index()
    {
        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->getAllUsers($_ENV['AUTH0_DOMAIN'], $accessToken);
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/users", name="create_user", methods={"POST"})
     * @param Request $request
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $user = new CreateUser();
        $user->name = $request->request->get('name');
        $user->email = $request->request->get('email');
        $user->password = $request->request->get('password');

        $errors = $this->validator->validate($user);

        if ($errors->count() > 0) {
            return new JsonResponse([
                'code' => 400,
                'message' => ErrorBeautifier::beautifyAsArray($errors)
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->createUser($_ENV['AUTH0_DOMAIN'], $accessToken, $user);
        } catch (ClientException $exception){
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/users/{user_id}", name="update_user", methods={"PUT"})
     * @param Request $request
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @return JsonResponse
     */
    public function update(Request $request)
    {
        $user = new UpdateUser();
        $user->id = $request->get('user_id');
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = $request->get('password');

        $errors = $this->validator->validate($user);

        if ($errors->count() > 0) {
            return new JsonResponse([
                'code' => 400,
                'message' => ErrorBeautifier::beautifyAsArray($errors)
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->updateUser($_ENV['AUTH0_DOMAIN'], $accessToken, $user);
        } catch (ClientException $exception){
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);

    }

    /**
     * @Route("api/v1/users/{user_id}", name="delete_user", methods={"DELETE"})
     * @param Request $request
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        if (!$request->get('user_id')) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'user id is required'
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->deleteUser($_ENV['AUTH0_DOMAIN'], $accessToken, $request->get('user_id'));
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/users/{user_id}/roles", name="get_user_roles", methods={"GET"})
     * @param Request $request
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @return JsonResponse
     */
    public function roles(Request $request)
    {
        if (!$request->get('user_id')) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'user id is required'
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->getUserRoles($_ENV['AUTH0_DOMAIN'], $accessToken, $request->get('user_id'));
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/users/{user_id}/roles", name="assign_user_roles", methods={"POST"})
     * @param Request $request
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @return JsonResponse
     */
    public function assignRoles(Request $request)
    {
        if (!$request->get('user_id')) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'user id is required'
            ], 400);
        }

        if (!$request->request->has('roles')) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'roles is required'
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->assignUserRoles(
                $_ENV['AUTH0_DOMAIN'],
                $accessToken,
                $request->get('user_id'),
                $request->get('roles')
            );
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/users/{user_id}/roles", name="remove_user_roles", methods={"DELETE"})
     * @param Request $request
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @return JsonResponse
     */
    public function removeRoles(Request $request)
    {
        if (!$request->get('user_id')) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'user id is required'
            ], 400);
        }

        if (!$request->request->has('roles')) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'roles is required'
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->removeRolesFromUser(
                $_ENV['AUTH0_DOMAIN'],
                $accessToken,
                $request->get('user_id'),
                $request->get('roles')
            );
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }
}