<?php

namespace App\Controller;

use Craft\Client\TasksClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class PdaController extends AbstractController
{
    private $client;

    public function __construct()
    {
        $this->client = new TasksClient($_ENV['TASKS_HOST']);
    }

    /**
     * @Route(name="pda_login", methods={"GET"}, path="api/v1/pda/login")
     */
    public function login(Request $request)
    {
        $response = $this->client->login([
            'error' => $request->get('error')
        ]);

        return new Response($response->getBody()->getContents(), $response->getStatusCode());
    }

    /**
     * @Route(name="pda_authenticate", methods={"POST"}, path="api/v1/pda/authenticate")
     */
    public function authenticate(Request $request)
    {
        $response = $this->forward('App\Controller\AuthController::login', [
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password')
        ]);

        if ($response->getStatusCode() != 200) {
            return $this->redirectToRoute('pda_login', ['error' => true]);
        }

        return new JsonResponse($response->getContent(), Response::HTTP_OK, [], true);
    }

    /**
     * @Route(name="pda_operations", methods={"GET"}, path="api/v1/pda/operations")
     * @IsGranted({"ROLE_PROCESSOR", "ROLE_WAREHOUSE_WORKER"})
     */
    public function operations(Request $request)
    {
        $response = $this->client->operations();

        return new Response($response->getBody()->getContents(), $response->getStatusCode());
    }

    /**
     * @Route(name="pda_picking", methods={"GET"}, path="api/v1/pda/picking")
     * @IsGranted({"ROLE_PROCESSOR", "ROLE_WAREHOUSE_WORKER"})
     */
    public function picking(Request $request)
    {
        $response = $this->client->picking();

        return new Response($response->getBody()->getContents(), $response->getStatusCode());
    }

    /**
     * @Route(name="pda_replenishment", methods={"GET"}, path="api/v1/pda/replenishment")
     * @IsGranted({"ROLE_PROCESSOR", "ROLE_WAREHOUSE_WORKER"})
     */
    public function replenishment(Request $request)
    {
        $response = $this->client->replenishment();

        return new Response($response->getBody()->getContents(), $response->getStatusCode());
    }

    /**
     * @Route(name="pda_order_picking", methods={"GET"}, path="api/v1/pda/order-picking/scan-location")
     * @IsGranted({"ROLE_PROCESSOR", "ROLE_WAREHOUSE_WORKER"})
     */
    public function orderPickingScanLocation(Request $request)
    {
        $response = $this->client->orderPicking($this->getUser()->getUserId());

        return new Response($response->getBody()->getContents(), $response->getStatusCode());
    }

    /**
     * @Route(name="pda_order_picking", methods={"POST"}, path="api/v1/pda/order-picking/scan-location")
     * @IsGranted({"ROLE_PROCESSOR", "ROLE_WAREHOUSE_WORKER"})
     */
    public function orderPickingValidateScanLocation(Request $request)
    {
        $response = $this->client->orderPicking($this->getUser()->getUserId());

        return new Response($response->getBody()->getContents(), $response->getStatusCode());
    }
}