<?php

namespace App\Controller;

use Craft\Client\StockClient;
use Craft\Dto\Container as ContainerDto;
use Craft\Utility\ErrorBeautifier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ContainersController extends AbstractController
{
    private $validator;
    private $stockClient;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->stockClient = new StockClient($_ENV['STOCK_HOST']);
    }

    /**
     * @Route("api/v1/containers", name="all_containers", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;

        $response = $this->stockClient->getAllContainers($page);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/containers", name="create_container", methods={"POST"})
     * @param Request $request
     * @IsGranted({"ROLE_PROCESSOR"})
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $container = new ContainerDto(json_decode($request->getContent(), true));

        $errors = $this->validator->validate($container);

        if ($errors->count() > 0) {
            return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $response = $this->stockClient->createContainer($container);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/containers/{containerId}", name="get_container", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param int $containerId
     * @return JsonResponse
     */
    public function show(int $containerId)
    {
        $response = $this->stockClient->getContainer($containerId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/containers/{containerId}", name="delete_container", methods={"DELETE"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param int $containerId
     * @return JsonResponse
     */
    public function delete(int $containerId)
    {
        $response = $this->stockClient->deleteContainer($containerId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }
}
