<?php

namespace App\Controller;

use Craft\Client\StockClient;
use Craft\Dto\Shelf as ShelfDto;
use Craft\Utility\ErrorBeautifier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ShelvesController extends AbstractController
{
    private $validator;
    private $stockClient;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->stockClient = new StockClient($_ENV['STOCK_HOST']);
    }

    /**
     * @Route("api/v1/shelves", name="all_shelves", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;

        $response = $this->stockClient->getAllShelves($page);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/shelves", name="create_shelf", methods={"POST"})
     * @param Request $request
     * @IsGranted({"ROLE_PROCESSOR"})
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $shelfDto = new ShelfDto(json_decode($request->getContent(), true));

        $errors = $this->validator->validate($shelfDto);

        if ($errors->count() > 0) {
            return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $response = $this->stockClient->createShelf($shelfDto);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/shelves/{shelfId}", name="get_shelf", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param int $shelfId
     * @return JsonResponse
     */
    public function show(int $shelfId)
    {
        $response = $this->stockClient->getShelf($shelfId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/shelves/{shelfId}", name="delete_shelf", methods={"DELETE"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param int $shelfId
     * @return JsonResponse
     */
    public function delete(int $shelfId)
    {
        $response = $this->stockClient->deleteShelf($shelfId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }
}
