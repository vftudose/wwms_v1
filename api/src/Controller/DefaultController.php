<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/api/v1", name="main_route", methods={"GET"})
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        return new JsonResponse([
            'status' => 'success',
            'message' => 'Welcome to Craft Wms Api, access docs.craft-wms.com to see the api documentation'
        ], 200);
    }
}