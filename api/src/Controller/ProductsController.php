<?php

namespace App\Controller;

use Craft\Dto\Product as ProductDto;
use Craft\Client\ProductsClient;
use Craft\Utility\ErrorBeautifier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductsController extends AbstractController
{
    private $productsClient;
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->productsClient = new ProductsClient($_ENV['PRODUCTS_HOST']);
        $this->validator = $validator;
    }

    /**
     * @Route("api/v1/products", name="create_product", methods={"POST"})
     * @param Request $request
     * @IsGranted({"ROLE_PROCESSOR"})
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $productDto = new ProductDto(json_decode($request->getContent(), true));

        $errors = $this->validator->validate($productDto);

        if ($errors->count() > 0) {
            return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $response = $this->productsClient->create($productDto);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/products/{productId}", name="update_product", methods={"PUT"})
     * @param Request $request
     * @param $productId
     * @return JsonResponse
     * @IsGranted({"ROLE_PROCESSOR"})
     */
    public function update(Request $request, $productId)
    {
        $productDto = new ProductDto(json_decode($request->getContent(), true));

        $errors = $this->validator->validate($productDto);

        if ($errors->count() > 0) {
            return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $response = $this->productsClient->update($productId, $productDto);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/products/{productId}", name="show_product", methods={"GET"})
     * @param $productId
     * @return JsonResponse
     * @IsGranted({"ROLE_PROCESSOR"})
     */
    public function show($productId)
    {
        $response = $this->productsClient->show($productId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(),[], true);
    }

    /**
     * @Route("api/v1/products/{productId}", name="delete_product", methods={"DELETE"})
     * @param $productId
     * @return JsonResponse
     * @IsGranted({"ROLE_PROCESSOR"})
     */
    public function delete($productId)
    {
        $response = $this->productsClient->delete($productId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(),[], true);
    }
}