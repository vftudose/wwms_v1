<?php

namespace App\Controller;

use Craft\Client\OrsClient;
use Craft\Client\ProductsClient;
use Craft\Event\OrderCreatedEvent;
use Craft\Event\PublishableEvent;
use Craft\Dto\Order\Order as OrderDto;
use Craft\Utility\ErrorBeautifier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class OrdersController extends AbstractController
{
    private $validator;
    private $orsClient;
    private $productsClient;
    private $dispatcher;

    public function __construct(
        ValidatorInterface $validator,
        EventDispatcherInterface $dispatcher
    )
    {
        $this->validator = $validator;
        $this->dispatcher = $dispatcher;
        $this->orsClient = new OrsClient($_ENV['ORDERS_HOST']);
        $this->productsClient = new ProductsClient($_ENV['PRODUCTS_HOST']);
    }

    /**
     * @Route("api/v1/orders", name="create_order", methods={"POST"})
     * @param Request $request
     * @IsGranted({"ROLE_PROCESSOR"})
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $orderDto = new OrderDto(json_decode($request->getContent(), true));

        $errors = $this->validator->validate($orderDto);

        if ($errors->count() > 0) {
            return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        foreach ($orderDto->products as $orderLine) {
            $errors = $this->validator->validate($orderLine);

            if ($errors->count() > 0) {
                return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        $orderProductsIds = $orderDto->getProductsIds();

        $response = $this->productsClient->getProductsByIds($orderProductsIds);

        $existingProductsIds = array_column(json_decode($response->getBody()->getContents(), true), 'id');

        $missingProductsIds = array_diff($orderProductsIds, $existingProductsIds);

        if (count($missingProductsIds) > 0) {
            return $this->json([
                'order' => sprintf('Order has undefined products: [%s]', implode(',', $missingProductsIds))
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $response = $this->orsClient->createOrder($orderDto);

        if ($response->getStatusCode() != Response::HTTP_OK) {
            return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
        }

        $order = json_decode($response->getBody()->getContents(), true);

        $orderDto->fromArray($order);

        $this->dispatcher->dispatch(new OrderCreatedEvent($orderDto), PublishableEvent::EVENT_NAME);

        return $this->json($order, Response::HTTP_OK);
    }

    /**
     * @Route("api/v1/orders/{orderId}", name="get_order", methods={"GET"})
     * @param int $orderId
     * @IsGranted({"ROLE_PROCESSOR"})
     * @return JsonResponse
     */
    public function show($orderId)
    {
        $response = $this->orsClient->getOrder($orderId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/orders", name="all_orders", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;

        $response = $this->orsClient->getAllOrders($page);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }
}