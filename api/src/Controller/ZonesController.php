<?php

namespace App\Controller;

use Craft\Client\StockClient;
use Craft\Dto\Zone as ZoneDto;
use Craft\Utility\ErrorBeautifier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ZonesController extends AbstractController
{
    private $validator;
    private $stockClient;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->stockClient = new StockClient($_ENV['STOCK_HOST']);
    }

    /**
     * @Route("api/v1/zones", name="all_zones", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;

        $response = $this->stockClient->getAllZones($page);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/zones", name="create_zone", methods={"POST"})
     * @param Request $request
     * @IsGranted({"ROLE_PROCESSOR"})
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $zoneDto = new ZoneDto(json_decode($request->getContent(), true));

        $response = $this->stockClient->getZoneByName($zoneDto->name);

        $zones = json_decode($response->getBody()->getContents(), true);

        if (count($zones['data'])) {
            return $this->json([
                'zone' => sprintf('Zone with name %s already exists', $zoneDto->name)
            ], Response::HTTP_BAD_REQUEST);
        }

        $errors = $this->validator->validate($zoneDto);

        if ($errors->count() > 0) {
            return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $response = $this->stockClient->createZone($zoneDto);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/zones/{zoneId}", name="get_zone", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param int $zoneId
     * @return JsonResponse
     */
    public function show(int $zoneId)
    {
        $response = $this->stockClient->getZone($zoneId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/zones/{zoneId}", name="delete_zone", methods={"DELETE"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param int $zoneId
     * @return JsonResponse
     */
    public function delete(int $zoneId)
    {
        $response = $this->stockClient->deleteZone($zoneId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }
}
