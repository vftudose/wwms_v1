<?php

namespace App\Controller;

use App\Security\Auth0ManagementClient\Auth0Management;
use App\Security\Auth0ManagementClient\Dto\CreateRole;
use App\Security\Auth0ManagementClient\Dto\UpdateRole;
use Craft\Utility\ErrorBeautifier;
use GuzzleHttp\Exception\ClientException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RolesController extends AbstractController
{
    private $validator;
    private $auth0Management;

    public function __construct(
        ValidatorInterface $validator,
        Auth0Management $auth0Management
    ) {
        $this->validator = $validator;
        $this->auth0Management = $auth0Management;
    }

    /**
     * @Route("api/v1/roles", name="get_roles", methods={"GET"})
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @return JsonResponse
     */
    public function index()
    {
        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->getAllRoles($_ENV['AUTH0_DOMAIN'], $accessToken);
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);

            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/roles", name="create_role", methods={"POST"})
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $role = new CreateRole();
        $role->name = $request->get('name');
        $role->description = $request->get('description');

        $errors = $this->validator->validate($role);

        if ($errors->count() > 0) {
            return new JsonResponse([
                'code' => 400,
                'message' => ErrorBeautifier::beautifyAsArray($errors)
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->createRole($_ENV['AUTH0_DOMAIN'], $accessToken, $role);
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/roles/{role_id}", name="get_role", methods={"GET"})
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
        if (!$request->get('role_id')) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'role_id is required'
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->getRole($_ENV['AUTH0_DOMAIN'], $accessToken, $request->get('role_id'));
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/roles/{role_id}", name="update_role", methods={"PUT"})
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request)
    {
        $role = new UpdateRole();
        $role->id = $request->get('role_id');
        $role->name = $request->get('name');
        $role->description = $request->get('description');

        $errors = $this->validator->validate($role);

        if ($errors->count() > 0) {
            return new JsonResponse([
                'code' => 400,
                'message' => ErrorBeautifier::beautifyAsArray($errors)
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->updateRole($_ENV['AUTH0_DOMAIN'], $accessToken, $role);
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/roles/{role_id}", name="delete_role", methods={"DELETE"})
     * @IsGranted({"ROLE_SUPER_ADMIN"})
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        if (!$request->get('role_id')) {
            return new JsonResponse([
                'code' => 400,
                'message' => 'role_id is required'
            ], 400);
        }

        $accessToken = $this->auth0Management->getAccessToken();

        try {
            $response = $this->auth0Management->deleteRole($_ENV['AUTH0_DOMAIN'], $accessToken, $request->get('role_id'));
        } catch (ClientException $exception) {
            $data = json_decode($exception->getResponse()->getBody()->getContents(), true);
            return new JsonResponse([
                'code' => $data['statusCode'],
                'message' => $data['message']
            ], $data['statusCode']);
        }

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }
}