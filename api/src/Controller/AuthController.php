<?php

namespace App\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    /**
     * @Route("api/v1/authenticate", name="authenticate")
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $email = $request->request->get('email');
        $password = $request->request->get('password');

        $guzzle = new Client();

        $response = $guzzle->post("https://{$_ENV['AUTH0_DOMAIN']}/oauth/token", [
            RequestOptions::FORM_PARAMS => [
                'grant_type' => 'password',
                'username' => $email,
                'password' => $password,
                'audience' => $_ENV['AUTH0_AUDIENCE'],
                'scope' => '',
                'client_id' => $_ENV['AUTH0_CLIENT_ID'],
                'client_secret' => $_ENV['AUTH0_CLIENT_SECRET']
            ]
        ]);

        return new JsonResponse($response->getBody()->getContents(),200, [], true);
    }
}
