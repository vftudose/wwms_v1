<?php

namespace App\Controller;

use Craft\Client\StockClient;
use Craft\Dto\Location as LocationDto;
use Craft\Utility\ErrorBeautifier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LocationsController extends AbstractController
{
    private $validator;
    private $stockClient;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->stockClient = new StockClient($_ENV['STOCK_HOST']);
    }

    /**
     * @Route("api/v1/locations", name="all_locations", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;

        $response = $this->stockClient->getAllLocations($page);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/locations", name="create_location", methods={"POST"})
     * @param Request $request
     * @IsGranted({"ROLE_PROCESSOR"})
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $locationDto = new LocationDto(json_decode($request->getContent(), true));

        if ($locationDto->type == LocationDto::TYPE_FIXED && $locationDto->shelfId == null) {
            return $this->json(['shelf-id' => 'Property shelf-id is required'], Response::HTTP_BAD_REQUEST);
        }

        if ($locationDto->type == LocationDto::TYPE_FIXED) {
            $response = $this->stockClient->getShelf($locationDto->shelfId);

            if ($response->getStatusCode() != Response::HTTP_OK) {
                return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
            }
        }

        $errors = $this->validator->validate($locationDto);

        if ($errors->count() > 0) {
            return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $response = $this->stockClient->createLocation($locationDto);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/locations/{locationId}", name="get_location", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param int $locationId
     * @return JsonResponse
     */
    public function show(int $locationId)
    {
        $response = $this->stockClient->getLocation($locationId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/locations/{locationId}", name="delete_location", methods={"DELETE"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param int $locationId
     * @return JsonResponse
     */
    public function delete(int $locationId)
    {
        $response = $this->stockClient->deleteLocation($locationId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }
}
