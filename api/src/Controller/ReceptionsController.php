<?php

namespace App\Controller;

use Craft\Client\OrsClient;
use Craft\Client\ProductsClient;
use Craft\Dto\Reception as ReceptionDto;
use Craft\Dto\ReceptionLine;
use Craft\Event\PublishableEvent;
use Craft\Event\ReceptionCreatedEvent;
use Craft\Utility\ErrorBeautifier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ReceptionsController extends AbstractController
{
    private $validator;
    private $orsClient;
    private $productsClient;
    private $dispatcher;

    public function __construct(
        ValidatorInterface $validator,
        EventDispatcherInterface $dispatcher
    ) {
        $this->validator = $validator;
        $this->dispatcher = $dispatcher;
        $this->orsClient = new OrsClient($_ENV['ORDERS_HOST']);
        $this->productsClient = new ProductsClient($_ENV['PRODUCTS_HOST']);
    }

    /**
     * @Route("api/v1/receptions", name="create_reception", methods={"POST"})
     * @param Request $request
     * @IsGranted({"ROLE_PROCESSOR"})
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $receptionDto = new ReceptionDto(json_decode($request->getContent(), true));

        $errors = $this->validator->validate($receptionDto);

        if ($errors->count() > 0) {
            return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /** @var ReceptionLine $receptionLine */
        foreach ($receptionDto->products as $receptionLine) {
            $errors = $this->validator->validate($receptionLine);

            if ($errors->count() > 0) {
                return $this->json(ErrorBeautifier::beautifyAsArray($errors), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        $receptionProductIds = $receptionDto->getProductsIds();

        $response = $this->productsClient->getProductsByIds($receptionProductIds);

        $existingProductsIds = array_column(json_decode($response->getBody()->getContents(), true), 'id');

        $missingProductsIds = array_diff($receptionProductIds, $existingProductsIds);

        if (count($missingProductsIds) > 0) {
            return $this->json([
                'reception' => sprintf('Reception has undefined products: [%s]', implode(',', $missingProductsIds))
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $response = $this->orsClient->createReception($receptionDto);

        if ($response->getStatusCode() != Response::HTTP_OK) {
            return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
        }

        $reception = json_decode($response->getBody()->getContents(), true);

        $receptionDto->fromArray($reception);

        $this->dispatcher->dispatch(new ReceptionCreatedEvent($receptionDto), PublishableEvent::EVENT_NAME);

        return $this->json($reception, Response::HTTP_OK);
    }

    /**
     * @Route("api/v1/receptions/{receptionId}", name="show_reception", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param $receptionId
     * @return JsonResponse
     */
    public function show($receptionId)
    {
        $response = $this->orsClient->getReception($receptionId);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }

    /**
     * @Route("api/v1/receptions", name="all_receptions", methods={"GET"})
     * @IsGranted({"ROLE_PROCESSOR"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;

        $response = $this->orsClient->getAllReceptions($page);

        return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
    }
}