<?php

namespace App\EventListener;

use Auth0\SDK\Exception\CoreException;
use Auth0\SDK\Exception\InvalidTokenException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AuthenticationExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        $code = $exception instanceof CoreException || $exception instanceOf InvalidTokenException ? 401 : 500;

        $message = $exception->getMessage();

        $ex = $exception->getPrevious();
        if ($ex instanceof AccessDeniedException) {
            $message = "Access Denied";
            $code = 401;
        }

        $responseData = [
            'error' => [
                'code' => $code,
                'message' => $message
            ]
        ];

        $event->setResponse(new JsonResponse($responseData, $code));
    }
}