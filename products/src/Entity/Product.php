<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="product", uniqueConstraints={@ORM\UniqueConstraint(columns={"sku"})})
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sku;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ProductDimension", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $dimensions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductBarcode", mappedBy="product", orphanRemoval=true, cascade={"persist"})
     */
    private $barcodes;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $updated_at;

    public function __construct()
    {
        $this->barcodes = new ArrayCollection();
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getDimension(): ?ProductDimension
    {
        return $this->dimensions;
    }

    public function setDimension(ProductDimension $dimensions): self
    {
        $this->dimensions = $dimensions;

        return $this;
    }

    /**
     * @return Collection|ProductBarcode[]
     */
    public function getBarcodes(): Collection
    {
        return $this->barcodes;
    }

    public function addBarcode(ProductBarcode $barcode): self
    {
        if (!$this->barcodes->contains($barcode)) {
            $this->barcodes[] = $barcode;
            $barcode->setProduct($this);
        }

        return $this;
    }

    public function removeBarcode(ProductBarcode $barcode): self
    {
        if ($this->barcodes->contains($barcode)) {
            $this->barcodes->removeElement($barcode);
            // set the owning side to null (unless already changed)
            if ($barcode->getProduct() === $this) {
                $barcode->setProduct(null);
            }
        }

        return $this;
    }

    public function getEanBarcode():? ProductBarcode
    {
        foreach ($this->getBarcodes() as $barcode) {
            if ($barcode->isEAN()) {
                return $barcode;
            }
        }

        return null;
    }

    public function getUpcBarcode():? ProductBarcode
    {
        foreach ($this->getBarcodes() as $barcode) {
            if ($barcode->isUPC()) {
                return $barcode;
            }
        }

        return null;
    }

    public function getIsbnBarcode():? ProductBarcode
    {
        foreach ($this->getBarcodes() as $barcode) {
            if ($barcode->isISBN()) {
                return $barcode;
            }
        }

        return null;
    }
}
