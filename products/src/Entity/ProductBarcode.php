<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductBarcodeRepository")
 * @ORM\Table(name="product_barcode",uniqueConstraints={@ORM\UniqueConstraint(name="code_unique", columns={"product_id", "barcode", "type"})})
 */
class ProductBarcode
{
    const TYPE_EAN = 'EAN';
    const TYPE_UPC = 'UPC';
    const TYPE_ISBN = 'ISBN';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $barcode;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('UPC', 'EAN', 'ISBN')")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="barcodes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setBarcode(string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function isEAN()
    {
        return $this->getType() == self::TYPE_EAN;
    }

    public function isUPC()
    {
        return $this->getType() == self::TYPE_UPC;
    }

    public function isISBN()
    {
        return $this->getType() == self::TYPE_ISBN;
    }
}
