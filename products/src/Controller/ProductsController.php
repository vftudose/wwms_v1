<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Craft\Dto\Product as ProductDto;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductsController extends AbstractController
{
    private $validator;
    private $productRepository;

    public function __construct(
        ValidatorInterface $validator,
        ProductRepository $productRepository
    )
    {
        $this->validator = $validator;
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("products", name="all_products", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;
        $ids = $request->query->has('ids') ? $request->query->get('ids') : [];
        $barcode = $request->query->has('barcode') ? $request->query->get('barcode') : null;

        $products = $this->productRepository->getProductsByIdsPaginated($ids, $page, $barcode);

        $data = [];

        /** @var Product $product */
        foreach ($products as $product) {
            $data[] = [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'sku' => $product->getSku(),
                'description' => $product->getDescription(),
                'dimensions' => [
                    'width' => $product->getDimension()->getWidth(),
                    'height' => $product->getDimension()->getHeight(),
                    'length' => $product->getDimension()->getLength(),
                    'weight' => $product->getDimension()->getWeight(),
                ],
                'barcodes' => [
                    'ean' => $product->getEanBarcode() != null ? $product->getEanBarcode()->getBarcode() : null,
                    'upc' => $product->getUpcBarcode() != null ? $product->getUpcBarcode()->getBarcode() : null,
                    'isbn' => $product->getIsbnBarcode() != null ? $product->getIsbnBarcode()->getBarcode() : null
                ],
                'created_at' => $product->getCreatedAt()
            ];
        }

        return $this->json($data, Response::HTTP_OK);
    }

    /**
     * @Route("products", name="create_product", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function create(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        $product = $this->productRepository->getProductBySku($content['sku']);

        if ($product) {
            return $this->json([
                'product' => sprintf('Product with sku %s already exists', $content['sku'])
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $productDTO = new ProductDto($content);

        try {
            $product = $this->productRepository->createProduct($productDTO);
        } catch (Exception $exception) {
            return $this->json([
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json([
            'id' => $product->getId(),
            'name' => $product->getName(),
            'sku' => $product->getSku(),
            'description' => $product->getDescription(),
            'dimensions' => [
                'width' => $product->getDimension()->getWidth(),
                'height' => $product->getDimension()->getHeight(),
                'length' => $product->getDimension()->getLength(),
                'weight' => $product->getDimension()->getWeight(),
            ],
            'barcodes' => [
                'ean' => $product->getEanBarcode() != null ? $product->getEanBarcode()->getBarcode() : null,
                'upc' => $product->getUpcBarcode() != null ? $product->getUpcBarcode()->getBarcode() : null,
                'isbn' => $product->getIsbnBarcode() != null ? $product->getIsbnBarcode()->getBarcode() : null
            ],
            'created_at' => $product->getCreatedAt()
        ], Response::HTTP_OK);
    }

    /**
     * @Route("products/{productId}", name="update_product", methods={"PUT"})
     * @param Request $request
     * @param $productId
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, $productId)
    {
        $content = json_decode($request->getContent(), true);

        $product = $this->productRepository->find($productId);

        if ($product == null) {
            return $this->json(['product' => sprintf('product with id %s not found', $productId)], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /** @var Product $product */
        $productWithSameSku = $this->productRepository->getProductBySkuWhereProductIdNot($content['sku'], $productId);

        if ($productWithSameSku) {
            return $this->json(['product' => sprintf('product with sku %s already exist', $content['sku'])], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $productDTO = new ProductDto($content);

        try {
            $this->productRepository->updateProduct($productDTO, $product);
        } catch (Exception $exception) {
            return $this->json([
                'message' => $exception->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->json([
            'id' => $product->getId(),
            'name' => $product->getName(),
            'sku' => $product->getSku(),
            'description' => $product->getDescription(),
            'dimensions' => [
                'width' => $product->getDimension()->getWidth(),
                'height' => $product->getDimension()->getHeight(),
                'length' => $product->getDimension()->getLength(),
                'weight' => $product->getDimension()->getWeight(),
            ],
            'barcodes' => [
                'ean' => $product->getEanBarcode() != null ? $product->getEanBarcode()->getBarcode() : null,
                'upc' => $product->getUpcBarcode() != null ? $product->getUpcBarcode()->getBarcode() : null,
                'isbn' => $product->getIsbnBarcode() != null ? $product->getIsbnBarcode()->getBarcode() : null
            ],
            'created_at' => $product->getCreatedAt()
        ], Response::HTTP_OK);
    }

    /**
     * @Route("products/{productId}", name="show_product", methods={"GET"})
     * @param $productId
     * @return JsonResponse
     * @throws Exception
     */
    public function show($productId)
    {
        $product = $this->productRepository->find($productId);

        if ($product == null) {
            return $this->json([
                'message' => sprintf('product with id %s not found', $productId)
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->json([
            'id' => $product->getId(),
            'name' => $product->getName(),
            'sku' => $product->getSku(),
            'description' => $product->getDescription(),
            'dimensions' => [
                'width' => $product->getDimension()->getWidth(),
                'height' => $product->getDimension()->getHeight(),
                'length' => $product->getDimension()->getLength(),
                'weight' => $product->getDimension()->getWeight(),
            ],
            'barcodes' => [
                'ean' => $product->getEanBarcode() != null ? $product->getEanBarcode()->getBarcode() : null,
                'upc' => $product->getUpcBarcode() != null ? $product->getUpcBarcode()->getBarcode() : null,
                'isbn' => $product->getIsbnBarcode() != null ? $product->getIsbnBarcode()->getBarcode() : null
            ],
            'created_at' => $product->getCreatedAt()
        ], Response::HTTP_OK);
    }

    /**
     * @Route("products/{productId}", name="delete_product", methods={"DELETE"})
     * @param $productId
     * @return JsonResponse
     * @throws Exception
     */
    public function delete($productId)
    {
        $product = $this->productRepository->find($productId);

        if ($product == null) {
            return $this->json([
                'product' => sprintf('product with id %s not found', $productId)
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $this->productRepository->delete($product);
        } catch (Exception $exception) {
            return $this->json([
                'product' => $exception->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
