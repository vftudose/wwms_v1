<?php

namespace App\Repository;

use App\Entity\ProductDimension;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductDimension|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductDimension|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductDimension[]    findAll()
 * @method ProductDimension[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductDimensionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductDimension::class);
    }
}
