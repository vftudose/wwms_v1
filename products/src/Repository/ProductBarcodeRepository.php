<?php

namespace App\Repository;

use App\Entity\ProductBarcode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductBarcode|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductBarcode|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductBarcode[]    findAll()
 * @method ProductBarcode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductBarcodeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductBarcode::class);
    }
}
