<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductBarcode;
use App\Entity\ProductDimension;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Craft\Dto\Product as ProductDto;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    const PER_PAGE = 100;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param ProductDto $productDTO
     * @return Product
     * @throws Exception
     */
    public function createProduct(ProductDto $productDTO): Product
    {
        /** @var EntityManager $em */
        $em = $this->getEntityManager();

        $timestamp = new DateTime();

        $em->beginTransaction();

        try {
            $dimension = new ProductDimension();
            $dimension->setWidth($productDTO->width)
                ->setHeight($productDTO->height)
                ->setLength($productDTO->length)
                ->setWeight($productDTO->weight)
                ->setCreatedAt($timestamp)
                ->setUpdatedAt($timestamp);

            $product = new Product();
            $product->setName($productDTO->name)
                ->setSku($productDTO->sku)
                ->setDescription($productDTO->description)
                ->setDimension($dimension)
                ->setUpdatedAt($timestamp)
                ->setCreatedAt($timestamp);

            if ($productDTO->ean != null) {
                $ean = new ProductBarcode();
                $ean->setProduct($product)
                    ->setType(ProductBarcode::TYPE_EAN)
                    ->setBarcode($productDTO->ean)
                    ->setCreatedAt($timestamp)
                    ->setUpdatedAt($timestamp);

                $product->addBarcode($ean);
            }

            if ($productDTO->upc != null) {
                $upc = new ProductBarcode();
                $upc->setProduct($product)
                    ->setType(ProductBarcode::TYPE_UPC)
                    ->setBarcode($productDTO->upc)
                    ->setCreatedAt($timestamp)
                    ->setUpdatedAt($timestamp);

                $product->addBarcode($upc);
            }

            if ($productDTO->isbn != null) {
                $isbn = new ProductBarcode();
                $isbn->setProduct($product)
                    ->setType(ProductBarcode::TYPE_ISBN)
                    ->setBarcode($productDTO->isbn)
                    ->setCreatedAt($timestamp)
                    ->setUpdatedAt($timestamp);

                $product->addBarcode($isbn);
            }

            $em->persist($product);
            $em->flush();
            $em->getConnection()->commit();

            return $product;
        } catch (Exception $exception) {
            $em->rollback();
            throw $exception;
        }
    }

    public function getProductBySku($sku)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.sku = :sku')
            ->setParameter('sku', $sku)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function updateProduct(ProductDto $productDTO, Product $product)
    {
        /** @var EntityManager $em */
        $em = $this->getEntityManager();

        $timestamp = new DateTime();

        $em->beginTransaction();

        try {
            $dimension = $product->getDimension();

            if ($dimension == null) {
                $dimension = new ProductDimension();
            }

            $dimension->setWidth($productDTO->width)
                ->setHeight($productDTO->height)
                ->setLength($productDTO->length)
                ->setWeight($productDTO->weight)
                ->setCreatedAt($timestamp)
                ->setUpdatedAt($timestamp);

            $product->setName($productDTO->name)
                ->setSku($productDTO->sku)
                ->setDescription($productDTO->description)
                ->setDimension($dimension)
                ->setUpdatedAt($timestamp)
                ->setCreatedAt($timestamp);

            if ($productDTO->ean != null) {

                $ean = $product->getEanBarcode();

                if ($ean == null) {
                    $ean = new ProductBarcode();
                }

                $ean->setProduct($product)
                    ->setType(ProductBarcode::TYPE_EAN)
                    ->setBarcode($productDTO->ean)
                    ->setCreatedAt($timestamp)
                    ->setUpdatedAt($timestamp);

                $em->persist($ean);
            }

            if ($productDTO->upc != null) {
                $upc = $product->getUpcBarcode();

                if ($upc == null) {
                    $upc = new ProductBarcode();
                }

                $upc->setProduct($product)
                    ->setType(ProductBarcode::TYPE_UPC)
                    ->setBarcode($productDTO->upc)
                    ->setCreatedAt($timestamp)
                    ->setUpdatedAt($timestamp);

                $em->persist($upc);
            }

            if ($productDTO->isbn != null) {

                $isbn = $product->getIsbnBarcode();

                if ($isbn == null) {
                    $isbn = new ProductBarcode();
                }

                $isbn->setProduct($product)
                    ->setType(ProductBarcode::TYPE_ISBN)
                    ->setBarcode($productDTO->isbn)
                    ->setCreatedAt($timestamp)
                    ->setUpdatedAt($timestamp);

                $em->persist($isbn);
            }

            $em->persist($product);
            $em->flush();
            $em->getConnection()->commit();
        } catch (Exception $exception) {
            $em->rollback();
            throw $exception;
        }

        return $product;
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Product $product)
    {
        $this->getEntityManager()->remove($product);

        $this->getEntityManager()->flush();
    }

    public function getProductBySkuWhereProductIdNot($sku, $productId)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.sku = :sku')->setParameter('sku', $sku)
            ->andWhere('p.id != :id')->setParameter('id', $productId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getProductsByIdsPaginated(array $ids, int $page, string $barcode = null)
    {
        $query = $this->createQueryBuilder('p')
            ->join(ProductDimension::class, 'pd', Join::ON)
            ->join(ProductBarcode::class, 'pb', Join::ON)
            ->setFirstResult(self::PER_PAGE * ($page - 1))
            ->setMaxResults(self::PER_PAGE);

        if (count($ids) > 0) {
            $query->where('p.id in (:ids)')->setParameter('ids', $ids);
        }

        if ($barcode) {
            $query->andWhere('pb.type = :type')->setParameter('type', ProductBarcode::TYPE_EAN)
                ->andWhere('pb.barcode = :barcode')->setParameter('barcode', $barcode);
        }

        return $query->getQuery()->getResult();
    }
}
