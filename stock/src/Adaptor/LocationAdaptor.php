<?php

namespace App\Adaptor;

use App\Entity\Location;

class LocationAdaptor
{
    public function toOutputFormat(Location $location): array
    {
        return [
            'id' => $location->getId(),
            'barcode' => $location->getBarcode(),
            'width' => $location->getWidth(),
            'height' => $location->getHeight(),
            'length' => $location->getHeight(),
            'shelf' => [
                'id' => $location->getShelf()->getId(),
                'width' => $location->getShelf()->getWidth(),
                'height' => $location->getShelf()->getHeight(),
                'length' => $location->getShelf()->getLength(),
                'created-at' => $location->getCreatedAt()
            ],
            'created-at' => $location->getCreatedAt()
        ];
    }

    public function fromArrayToOutputFormat(array $locations): array
    {
        $data = [];

        /** @var Location $location */
        foreach ($locations as $location) {
            $data[] = $this->toOutputFormat($location);
        }

        return  $data;
    }
}