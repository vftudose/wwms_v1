<?php

namespace App\Adaptor;

use App\Entity\Container;

class ContainerAdapter
{
    public function toOutputFormat(Container $container): array
    {
        return [
            'id' => $container->getId(),
            'barcode' => $container->getBarcode(),
            'type' => $container->getType(),
            'created-at' => $container->getCreatedAt()
        ];
    }

    public function fromArrayToOutputFormat(array $containers): array
    {
        $data = [];

        /** @var Container $container */
        foreach ($containers as $container) {
            $data[] = $this->toOutputFormat($container);
        }

        return  $data;
    }
}