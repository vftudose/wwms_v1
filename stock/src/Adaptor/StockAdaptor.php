<?php

namespace App\Adaptor;

use App\Entity\Stock;

class StockAdaptor
{
    public function toOutputFormat(Stock $stock): array
    {
        return [
            'id' => $stock->getId(),
            'location-id' => $stock->getLocation()->getId(),
            'container-id' => $stock->getContainer()->getId(),
            'product-id' => $stock->getProductId(),
            'quantity' => $stock->getQuantity(),
            'reserved-quantity' => $stock->getReservedQuantity()
        ];
    }

    public function fromArrayToOutputFormat(array $stockCollection): array
    {
        $data = [];

        foreach ($stockCollection as $shelf) {
            $data[] = $this->toOutputFormat($shelf);
        }

        return $data;
    }
}