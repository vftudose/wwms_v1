<?php

namespace App\Adaptor;

use App\Entity\Shelf;

class ShelfAdaptor
{
    public function toOutputFormat(Shelf $shelf): array
    {
        return [
            'id' => $shelf->getId(),
            'width' => $shelf->getWidth(),
            'height' => $shelf->getHeight(),
            'length' => $shelf->getLength(),
            'zone' => [
                'id' => $shelf->getZone()->getId(),
                'name' => $shelf->getZone()->getName(),
                'created-at' => $shelf->getZone()->getCreatedAt()
            ],
            'created-at' => $shelf->getCreatedAt()
        ];
    }

    public function fromArrayToOutputFormat(array $shelvesCollection): array
    {
        $data = [];

        /** @var Shelf $shelf */
        foreach ($shelvesCollection as $shelf)
        {
            $data[] = $this->toOutputFormat($shelf);
        }

        return $data;
    }
}