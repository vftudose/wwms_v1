<?php

namespace App\Adaptor;

use App\Entity\Zone;

class ZoneAdaptor
{
    public function toOutputFormat(Zone $zone): array
    {
        return [
            'id' => $zone->getId(),
            'name' => $zone->getName(),
            'type' => $zone->getType(),
            'created-at' => $zone->getCreatedAt()
        ];
    }

    public function fromArrayToOutputFormat(array $zones): array
    {
        $data = [];

        /** @var Zone $zone */
        foreach ($zones as $zone) {
            $data[] = $this->toOutputFormat($zone);
        }

        return $data;
    }
}