<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 * @ORM\Table(name="stock", uniqueConstraints={@ORM\UniqueConstraint(columns={"product_id", "location_id"})})
 */
class Stock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $productId;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    private $reservedQuantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", inversedBy="stocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Container", inversedBy="stocks")
     */
    private $container;

    public function __construct()
    {
        $this->quantity = 0;
        $this->reservedQuantity = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductId(): ?int
    {
        return $this->productId;
    }

    public function setProductId(int $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getContainer(): ?Container
    {
        return $this->container;
    }

    public function setContainer(?Container $container): self
    {
        $this->container = $container;

        return $this;
    }

    public function getReservedQuantity(): int
    {
        return $this->reservedQuantity;
    }

    public function reserve(): void
    {
        $this->reservedQuantity += 1;
    }

    public function unreserve(int $quantity = 1): void
    {
        if (($this->reservedQuantity - $quantity) < 0) {
            $this->reservedQuantity = 0;
        } else {
            $this->reservedQuantity -= $quantity;
        }
    }

    public function decrease(int $quantity): void
    {
        if (($this->quantity - $quantity) < 0) {
            $this->quantity = 0;
        } else {
            $this->quantity -= $quantity;
        }
    }

    public function increase(int $quantity): void
    {
        $this->quantity += $quantity;
    }
}
