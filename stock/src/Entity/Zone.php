<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZoneRepository")
 * @ORM\Table(name="zone", uniqueConstraints={@ORM\UniqueConstraint(columns={"name"})})
 */
class Zone
{
    const TYPE_STORAGE = 'storage';
    const TYPE_PICKING = 'picking';
    const TYPE_PACKAGE = 'package';
    const TYPE_EXIT = 'exit';

    const ALLOWED_TYPES = [
        self::TYPE_STORAGE,
        self::TYPE_PICKING,
        self::TYPE_PACKAGE,
        self::TYPE_EXIT
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shelf", mappedBy="zone", orphanRemoval=true)
     */
    private $shelves;

    public function __construct()
    {
        $this->shelves = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, self::ALLOWED_TYPES)) {
            throw new Exception(sprintf('Type %s is not allowed', $type));
        }

        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Shelf[]
     */
    public function getShelves(): Collection
    {
        return $this->shelves;
    }

    public function addShelf(Shelf $shelf): self
    {
        if (!$this->shelves->contains($shelf)) {
            $this->shelves[] = $shelf;
            $shelf->setZone($this);
        }

        return $this;
    }

    public function removeShelf(Shelf $shelf): self
    {
        if ($this->shelves->contains($shelf)) {
            $this->shelves->removeElement($shelf);
            // set the owning side to null (unless already changed)
            if ($shelf->getZone() === $this) {
                $shelf->setZone(null);
            }
        }

        return $this;
    }
}
