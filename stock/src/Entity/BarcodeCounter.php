<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BarcodeCounterRepository")
 */
class BarcodeCounter
{
    const MSI = 'msi';
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=55)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private $counter;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCounter(): int
    {
        return $this->counter;
    }

    public function incrementByOne(): void
    {
        $this->counter = $this->counter + 1;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function initCounter()
    {
        $this->counter = 0;
    }
}
