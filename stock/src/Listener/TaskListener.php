<?php

namespace App\Listener;

use App\Repository\ContainerRepository;
use App\Repository\LocationRepository;
use App\Repository\StockRepository;
use Craft\Event\TaskPickedEvent;
use Psr\Log\LoggerInterface;

class TaskListener
{
    private $stockRepository;
    private $containerRepository;
    private $locationRepository;
    private $logger;

    public function __construct(
        StockRepository $stockRepository,
        ContainerRepository $containerRepository,
        LocationRepository $locationRepository,
        LoggerInterface $logger
    ) {
        $this->stockRepository = $stockRepository;
        $this->containerRepository = $containerRepository;
        $this->locationRepository = $locationRepository;
        $this->logger = $logger;
    }

    public function onTaskPicked(TaskPickedEvent $taskPickedEvent): bool
    {
        $data = $taskPickedEvent->getData();

        $fromStock = $this->stockRepository->findOneBy([
            'location' => $data['location-id'],
            'productId' => $data['product-id']
        ]);

        $toStock = $this->stockRepository->findOneBy([
            'container' => $data['transit-container-id'],
            'productId' => $data['product-id']
        ]);

        if (!$toStock) {
            $container = $this->containerRepository->find($data['transit-container-id']);
            $location = $this->locationRepository->findTransitLocation();
            $toStock = $this->stockRepository->createStockLine($container, $location, $data['product-id']);
        }

        $this->stockRepository->moveQuantity($fromStock, $toStock);

        return true;
    }
}