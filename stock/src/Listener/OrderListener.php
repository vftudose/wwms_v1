<?php

namespace App\Listener;

use App\Entity\Stock;
use App\Repository\StockRepository;
use Craft\Dto\Order\Order;
use Craft\Dto\Order\OrderLine;
use Craft\Event\OrderCreatedEvent;
use Craft\Event\OrderReservedEvent;
use Craft\Event\OrderUnreservedEvent;
use Craft\Event\PublishableEvent;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class OrderListener
{
    private $stockRepository;
    private $dispatcher;
    private $logger;

    public function __construct(
        StockRepository $stockRepository,
        EventDispatcherInterface $dispatcher,
        LoggerInterface $logger
    )
    {
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
        $this->stockRepository = $stockRepository;
    }

    /**
     * @param OrderCreatedEvent $event
     * @return bool
     * @throws Exception
     */
    public function onCreatedOrder(OrderCreatedEvent $event): bool
    {
        $order = $event->getOrder();

        $this->stockRepository->beginTransaction();

        try {
            /** @var OrderLine $orderLine */
            foreach ($order->products as $orderLine) {
                /** @var Stock $stock */
                $stock = $this->stockRepository->findStockByProductIdAndAvailableQuantity($orderLine->productId, $orderLine->quantity);

                if (!$stock) {
                    throw new Exception(sprintf("No stock found for product with id %s", $orderLine->productId));
                }

                $this->stockRepository->reserveOrderLine($orderLine, $stock);

                $orderLine->reservedLocationId = $stock->getLocation()->getId();
            }

            $this->stockRepository->commit();

        } catch (Exception $exception) {
            $this->stockRepository->rollback();
            $this->logger->info($exception->getMessage());
            $this->dispatcher->dispatch(new OrderUnreservedEvent($order), PublishableEvent::EVENT_NAME);
            return true;
        }

        $this->dispatcher->dispatch(new OrderReservedEvent($order), PublishableEvent::EVENT_NAME);

        return true;
    }
}