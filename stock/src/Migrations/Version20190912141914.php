<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190912141914 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'insert counter for msi barcode';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT INTO barcode_counter(name, counter) VALUES(\'msi\', 0)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE FROM barcode_counter WHERE name = \'msi\'');
    }
}
