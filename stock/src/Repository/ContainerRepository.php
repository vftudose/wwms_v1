<?php

namespace App\Repository;

use App\Entity\Container;
use Craft\Dto\Container as ContainerDto;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Container|null find($id, $lockMode = null, $lockVersion = null)
 * @method Container|null findOneBy(array $criteria, array $orderBy = null)
 * @method Container[]    findAll()
 * @method Container[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContainerRepository extends ServiceEntityRepository
{
    const PER_PAGE = 100;

    private $barcodeCounterRepository;

    public function __construct(RegistryInterface $registry, BarcodeCounterRepository $barcodeCounterRepository)
    {
        parent::__construct($registry, Container::class);
        $this->barcodeCounterRepository = $barcodeCounterRepository;
    }

    public function createContainer(ContainerDto $containerDto, string $barcode)
    {
        $container = new Container();

        try {
            $container->setType($containerDto->type)
                ->setBarcode($barcode)
                ->setCreatedAt(new DateTime())
                ->setUpdatedAt(new DateTime());

            $this->_em->persist($container);
            $this->_em->flush();
        } catch (Exception $exception) {
            $this->_em->rollback();
            throw $exception;
        }

        return $container;
    }

    public function getContainersPaginated(int $page, string $barcode = null)
    {
        $query = $this->createQueryBuilder('c');

        if ($barcode) {
            $query->andWhere('c.barcode = :barcode')->setParameter('barcode', $barcode);
        }

        return $query->getQuery()
            ->setFirstResult(self::PER_PAGE * ($page - 1))
            ->setMaxResults(self::PER_PAGE)
            ->getResult();
    }

    public function deleteContainer(Container $container): void
    {
        $this->_em->remove($container);
        $this->_em->flush();
    }
}
