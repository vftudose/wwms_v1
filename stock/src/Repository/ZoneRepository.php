<?php

namespace App\Repository;

use App\Entity\Zone;
use \Craft\Dto\Zone as ZoneDto;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Zone|null find($id, $lockMode = null, $lockVersion = null)
 * @method Zone|null findOneBy(array $criteria, array $orderBy = null)
 * @method Zone[]    findAll()
 * @method Zone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ZoneRepository extends ServiceEntityRepository
{
    const PER_PAGE = 100;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Zone::class);
    }

    public function create(ZoneDto $zoneDto): Zone
    {
        $zone = new Zone();
        $zone->setName($zoneDto->name)
            ->setType($zoneDto->type)
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());

        $this->_em->persist($zone);
        $this->_em->flush();

        return $zone;
    }

    public function getZonesFilteredAndPaginated(int $page, string $name = null)
    {
        $query = $this->createQueryBuilder('z');

        if ($name) {
            $query->where('z.name = :name')->setParameter('name', $name);
        }

        return $query->getQuery()
            ->setFirstResult(self::PER_PAGE * ($page - 1))
            ->setMaxResults(self::PER_PAGE)
            ->getResult();
    }

    public function delete($zoneId)
    {
        $zone = $this->find($zoneId);

        $this->getEntityManager()->remove($zone);
        $this->getEntityManager()->flush();
    }
}
