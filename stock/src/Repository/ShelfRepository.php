<?php

namespace App\Repository;

use App\Entity\Shelf;
use App\Entity\Zone;
use Craft\Dto\Shelf as ShelfDto;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Shelf|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shelf|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shelf[]    findAll()
 * @method Shelf[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShelfRepository extends ServiceEntityRepository
{
    const PER_PAGE = 100;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Shelf::class);
    }

    public function createShelfOnZone(ShelfDto $shelfDto, Zone $zone): Shelf
    {
        $shelf = new Shelf();
        $shelf->setHeight($shelfDto->height)
            ->setWidth($shelfDto->width)
            ->setLength($shelfDto->length)
            ->setZone($zone)
            ->setCreatedAt(new DateTime())
            ->setUpdatedAt(new DateTime());

        $this->_em->persist($shelf);
        $this->_em->flush();

        return $shelf;
    }

    public function delete($shelfId)
    {
        $shelf = $this->find($shelfId);

        $this->_em->remove($shelf);
        $this->_em->flush();
    }

    public function getShelvesPaginated(int $page)
    {
        return $this->createQueryBuilder('s')
            ->getQuery()
            ->setFirstResult(self::PER_PAGE * ($page - 1))
            ->setMaxResults(self::PER_PAGE)
            ->getResult();
    }
}
