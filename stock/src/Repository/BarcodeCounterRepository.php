<?php

namespace App\Repository;

use App\Entity\BarcodeCounter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Exception;

/**
 * @method BarcodeCounter|null find($id, $lockMode = null, $lockVersion = null)
 * @method BarcodeCounter|null findOneBy(array $criteria, array $orderBy = null)
 * @method BarcodeCounter[]    findAll()
 * @method BarcodeCounter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BarcodeCounterRepository extends ServiceEntityRepository
{
    const MSI_BARCODE_LENGTH = 11;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BarcodeCounter::class);
    }

    public function generateAndIncrementMsiBarcode()
    {
        $msiBarcode = $this->findOneBy(['name' => BarcodeCounter::MSI]);
        
        if (!$msiBarcode) {
            throw new Exception('Missing barcode counter');
        }

        try {
            $msiBarcode->incrementByOne();
            $this->_em->persist($msiBarcode);
            $this->_em->flush();
        } catch (Exception $exception) {
            $this->_em->rollback();
            throw $exception;
        }

        return str_pad($msiBarcode->getCounter(), self::MSI_BARCODE_LENGTH, "0", STR_PAD_LEFT);
    }
}
