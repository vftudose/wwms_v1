<?php

namespace App\Repository;

use App\Entity\Location;
use App\Entity\Shelf;
use App\Entity\Stock;
use Craft\Dto\Location as LocationDto;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Location|null find($id, $lockMode = null, $lockVersion = null)
 * @method Location|null findOneBy(array $criteria, array $orderBy = null)
 * @method Location[]    findAll()
 * @method Location[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationRepository extends ServiceEntityRepository
{
    const PER_PAGE = 100;

    private $barcodeCounterRepository;

    public function __construct(
        RegistryInterface $registry,
        BarcodeCounterRepository $barcodeCounterRepository
    )
    {
        parent::__construct($registry, Location::class);
        $this->barcodeCounterRepository = $barcodeCounterRepository;
    }

    public function createLocationOnShelf(LocationDto $locationDto, Shelf $shelf, string $barcode): Location
    {
        $location = new Location();
        try {
            $location->setType($locationDto->type)
                ->setWidth($locationDto->width)
                ->setHeight($locationDto->height)
                ->setLength($locationDto->length)
                ->setBarcode($barcode)
                ->setUpdatedAt(new DateTime())
                ->setCreatedAt(new DateTime())
                ->setShelf($shelf);

            $this->_em->persist($location);
            $this->_em->flush();
        } catch (Exception $exception) {
            $this->_em->rollback();
            throw $exception;
        }

        return $location;
    }

    public function getLocationsPaginated(int $page, string $barcode = null)
    {
        $query = $this->createQueryBuilder('l');

        if ($barcode) {
            $query->andWhere('l.barcode = :barcode')->setParameter('barcode', $barcode);
        }

        return $query->getQuery()
            ->setFirstResult(self::PER_PAGE * ($page - 1))
            ->setMaxResults(self::PER_PAGE)
            ->getResult();
    }

    public function deleteLocation(Location $location): void
    {
        $this->_em->remove($location);
        $this->_em->flush($location);
    }

    public function getAvailableLocationThatFits(float $volume)
    {
        return $this->createQueryBuilder('l')
            ->where('(l.width * l.height * l.width) > :volume')
            ->setParameter('volume', $volume)
            ->getQuery()->getResult();
    }

    public function findTransitLocation()
    {
        return $this->createQueryBuilder('l')
            ->where('l.type = :type')->setParameter('type', Location::TYPE_TRANSIT)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
