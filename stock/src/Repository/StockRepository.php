<?php

namespace App\Repository;

use App\Entity\Container;
use App\Entity\Location;
use App\Entity\Stock;
use Craft\Dto\Order\OrderLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Stock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stock[]    findAll()
 * @method Stock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Stock::class);
    }

    /**
     * @param array $product
     * @param Stock $stock
     * @throws Exception
     */
    public function reserveOrderLine(OrderLine $orderLine, Stock $stock): void
    {
        for ($i = 0; $i < $orderLine->quantity; $i++) {
            $this->reserveStock($stock);
        }
    }

    public function commit()
    {
        $this->_em->flush();
        $this->_em->commit();
    }

    public function beginTransaction()
    {
        $this->_em->beginTransaction();
    }

    public function rollback()
    {
        $this->_em->rollback();
    }

    public function findStockByProductIdAndAvailableQuantity(int $productId, int $quantity)
    {
        return $this->createQueryBuilder('s')
            ->where('s.productId = :productId')
            ->andWhere('(s.quantity - s.reservedQuantity) >= :productQuantity')
            ->setParameter('productQuantity', $quantity)
            ->setParameter('productId', $productId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function reserveStock(Stock $stock)
    {
        $this->createQueryBuilder('s')->update()
            ->where('s.id = :id')->setParameter('id', $stock->getId())
            ->set('s.reservedQuantity', 's.reservedQuantity + 1')
            ->getQuery()
            ->getResult();

    }

    public function findStockForContainer(int $containerId)
    {
        return $this->createQueryBuilder('s')
            ->where('s.container = :containerId')->setParameter('containerId', $containerId)
            ->getQuery()
            ->getResult();
    }

    public function moveQuantity(Stock $fromStock, Stock $toStock, $quantity = 1)
    {
        $fromStock->unreserve($quantity);
        $fromStock->decrease($quantity);

        $toStock->increase($quantity);

        $this->_em->persist($fromStock);
        $this->_em->persist($toStock);
        $this->_em->flush();
    }

    public function createStockLine(Container $container, Location $location, $productId): Stock
    {
        $stock = new Stock();
        $stock->setContainer($container)
            ->setLocation($location)
            ->setProductId($productId)
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime());

        $this->_em->persist($stock);
        $this->_em->flush();

        return $stock;
    }
}
