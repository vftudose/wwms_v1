<?php

namespace App\Controller;

use App\Adaptor\ShelfAdaptor;
use App\Entity\Shelf;
use App\Repository\ShelfRepository;
use Craft\Dto\Shelf as ShelfDto;
use App\Repository\ZoneRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShelvesController extends AbstractController
{
    private $shelfRepository;
    private $zoneRepository;
    private $shelfAdaptor;

    public function __construct(
        ShelfRepository $shelfRepository,
        ZoneRepository $zoneRepository,
        ShelfAdaptor $shelfAdaptor
    )
    {
        $this->shelfRepository = $shelfRepository;
        $this->zoneRepository = $zoneRepository;
        $this->shelfAdaptor = $shelfAdaptor;
    }

    /**
     * @Route("shelves", name="all_shelves", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;

        $shelves = $this->shelfRepository->getShelvesPaginated($page);

        return $this->json([
            'total' => $this->shelfRepository->count([]),
            'page' => $page,
            'per_page' => ShelfRepository::PER_PAGE,
            'data' => $this->shelfAdaptor->fromArrayToOutputFormat($shelves)
        ], Response::HTTP_OK);
    }

    /**
     * @Route("shelves", name="create_shelf", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $shelfDto = new ShelfDto(json_decode($request->getContent(), true));

        $zone = $this->zoneRepository->find($shelfDto->zoneId);

        if (!$zone) {
            return $this->json([
                'zone' => sprintf("Zone with id %s not found", $shelfDto->zoneId)
            ], Response::HTTP_NOT_FOUND);
        }

        try {
            $shelf = $this->shelfRepository->createShelfOnZone($shelfDto, $zone);
        } catch (Exception $exception) {
            return $this->json([
                'status' => 'error',
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json($this->shelfAdaptor->toOutputFormat($shelf), Response::HTTP_OK);
    }

    /**
     * @Route("shelves/{shelfId}", name="show_shelf", methods={"GET"})
     * @param $shelfId
     * @return JsonResponse
     */
    public function show($shelfId)
    {
        $shelf = $this->shelfRepository->find($shelfId);

        if (!$shelf) {
            return $this->json([
                'shelf' => sprintf("Shelf with id %s not found", $shelfId)
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->json($this->shelfAdaptor->toOutputFormat($shelf));
    }

    /**
     * @Route("shelves/{shelfId}", name="delete_shelf", methods={"DELETE"})
     * @param $shelfId
     * @return JsonResponse
     */
    public function delete($shelfId)
    {
        $shelf = $this->shelfRepository->find($shelfId);

        if (!$shelf) {
            return $this->json([
                'shelf' => sprintf("Shelf with id %s not found", $shelfId)
            ], Response::HTTP_NOT_FOUND);
        }

        try {
            $this->shelfRepository->delete($shelfId);
        } catch (Exception $exception) {
            return $this->json([
                'status' => 'error',
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
