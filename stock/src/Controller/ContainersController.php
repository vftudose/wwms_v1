<?php

namespace App\Controller;

use App\Adaptor\ContainerAdapter;
use App\Adaptor\StockAdaptor;
use App\Listener\TaskListener;
use App\Repository\BarcodeCounterRepository;
use App\Repository\ContainerRepository;
use App\Repository\StockRepository;
use Craft\Client\StockClient;
use Craft\Dto\Container as ContainerDto;
use Craft\Dto\Task;
use Craft\Event\TaskPickedEvent;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContainersController extends AbstractController
{
    private $containerRepository;
    private $barcodeCounterRepository;
    private $stockRepository;
    private $containerAdapter;
    private $stockAdaptor;

    public function __construct(
        ContainerRepository $containerRepository,
        ContainerAdapter $containerAdapter,
        BarcodeCounterRepository $barcodeCounterRepository,
        StockRepository $stockRepository,
        StockAdaptor $stockAdaptor
    )
    {
        $this->containerRepository = $containerRepository;
        $this->containerAdapter = $containerAdapter;
        $this->barcodeCounterRepository = $barcodeCounterRepository;
        $this->stockRepository = $stockRepository;
        $this->stockAdaptor = $stockAdaptor;
    }

    /**
     * @Route("containers", name="all_containers", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;

        $barcode = $request->query->has('barcode') ? $request->query->get('barcode') : null;

        $containers = $this->containerRepository->getContainersPaginated($page, $barcode);

        return $this->json([
            'total' => $this->containerRepository->count([]),
            'page' => $page,
            'per_page' => ContainerRepository::PER_PAGE,
            'data' => $this->containerAdapter->fromArrayToOutputFormat($containers)
        ], Response::HTTP_OK);
    }

    /**
     * @Route("containers", name="create_container", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $containerDto = new ContainerDto(json_decode($request->getContent(), true));

        try {
            $barcode = $this->barcodeCounterRepository->generateAndIncrementMsiBarcode();
            $container = $this->containerRepository->createContainer($containerDto, $barcode);
        } catch (Exception $exception) {
            return $this->json([
                'status' => 'error',
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json($this->containerAdapter->toOutputFormat($container), Response::HTTP_OK);
    }

    /**
     * @Route("containers/{containerId}", name="show_container", methods={"GET"})
     * @param int $containerId
     * @return JsonResponse
     */
    public function show(int $containerId)
    {
        $container = $this->containerRepository->find($containerId);

        if (!$container) {
            return $this->json([
                'container' => sprintf("Container with id %s not found", $containerId)
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->json($this->containerAdapter->toOutputFormat($container), Response::HTTP_OK);
    }

    /**
     * @Route("containers/{containerId}", name="delete_container", methods={"DELETE"})
     * @param int $containerId
     * @return JsonResponse
     */
    public function delete(int $containerId)
    {
        $container = $this->containerRepository->find($containerId);

        if (!$container) {
            return $this->json([
                'container' => sprintf("Container with id %s not found", $containerId)
            ], Response::HTTP_NOT_FOUND);
        }

        try {
            $this->containerRepository->deleteContainer($container);
        } catch (Exception $exception) {
            return $this->json([
                'status' => 'error',
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("containers/{containerId}/stock", name="get_container_stock", methods={"GET"})
     * @param int $containerId
     * @return JsonResponse
     */
    public function stock(int $containerId)
    {
        $container = $this->containerRepository->find($containerId);

        if (!$container) {
            return $this->json([
                'container' => sprintf("Container with id %s not found", $containerId)
            ], Response::HTTP_NOT_FOUND);
        }

        $stocks = $this->stockRepository->findStockForContainer($containerId);

        if (!$stocks) {
            return $this->json([], Response::HTTP_OK);
        }

        return $this->json($this->stockAdaptor->fromArrayToOutputFormat($stocks), Response::HTTP_OK);
    }
}
