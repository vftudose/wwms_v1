<?php

namespace App\Controller;

use App\Adaptor\ZoneAdaptor;
use App\Repository\ZoneRepository;
use App\Service\StockService;
use Craft\Dto\Zone as ZoneDto;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ZonesController extends AbstractController
{
    private $zoneRepository;
    private $zoneAdaptor;

    public function __construct(
        ZoneRepository $zoneRepository,
        ZoneAdaptor $zoneAdaptor
    )
    {
        $this->zoneRepository = $zoneRepository;
        $this->zoneAdaptor = $zoneAdaptor;
    }

    /**
     * @Route("zones", name="all_zones", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;
        $name = $request->query->has('name') ? $request->query->get('name') : null;

        $zones = $this->zoneRepository->getZonesFilteredAndPaginated($page, $name);

        return $this->json([
            'total' => $this->zoneRepository->count([]),
            'page' => $page,
            'per_page' => ZoneRepository::PER_PAGE,
            'data' => $this->zoneAdaptor->fromArrayToOutputFormat($zones)
        ], Response::HTTP_OK);
    }

    /**
     * @Route("zones", name="create_zone", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $zoneDto = new ZoneDto(json_decode($request->getContent(), true));

        try {
            $zone = $this->zoneRepository->create($zoneDto);
        } catch (Exception $exception) {
            return $this->json([
                'status' => 'error',
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json($this->zoneAdaptor->toOutputFormat($zone), Response::HTTP_OK);
    }

    /**
     * @Route("zones/{zoneId}", name="show_zone", methods={"GET"})
     * @param $zoneId
     * @return JsonResponse
     */
    public function show($zoneId)
    {
        $zone = $this->zoneRepository->find($zoneId);

        if (!$zone) {
            return $this->json([
                'zone' => sprintf('Zone with id %s not found', $zoneId)
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->json($this->zoneAdaptor->toOutputFormat($zone), Response::HTTP_OK);
    }

    /**
     * @Route("zones/{zoneId}", name="delete_zone", methods={"DELETE"})
     * @param $zoneId
     * @return JsonResponse
     */
    public function delete($zoneId)
    {
        $zone = $this->zoneRepository->find($zoneId);

        if (!$zone) {
            return $this->json([
                'zone' => sprintf('Zone with id %s not found', $zoneId)
            ], Response::HTTP_NOT_FOUND);
        }

        $this->zoneRepository->delete($zoneId);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
