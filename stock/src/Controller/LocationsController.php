<?php

namespace App\Controller;

use App\Adaptor\LocationAdaptor;
use App\Entity\Location;
use App\Repository\BarcodeCounterRepository;
use App\Repository\LocationRepository;
use App\Repository\ShelfRepository;
use Craft\Client\ProductsClient;
use Craft\Dto\Location as LocationDto;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LocationsController extends AbstractController
{
    private $locationRepository;
    private $shelfRepository;
    private $locationAdaptor;
    private $barcodeCounterRepository;
    private $productsClient;

    public function __construct(
        LocationRepository $locationRepository,
        ShelfRepository $shelfRepository,
        BarcodeCounterRepository $barcodeCounterRepository,
        LocationAdaptor $locationAdaptor
    )
    {
        $this->shelfRepository = $shelfRepository;
        $this->locationRepository = $locationRepository;
        $this->barcodeCounterRepository = $barcodeCounterRepository;
        $this->locationAdaptor = $locationAdaptor;
        $this->productsClient = new ProductsClient($_ENV['PRODUCTS_HOST']);
    }

    /**
     * @Route("locations", name="all_locations", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;

        $barcode = $request->query->has('barcode') ? $request->query->get('barcode') : null;

        $locations = $this->locationRepository->getLocationsPaginated($page, $barcode);

        return $this->json([
            'total' => $this->locationRepository->count([]),
            'page' => $page,
            'per_page' => LocationRepository::PER_PAGE,
            'data' => $this->locationAdaptor->fromArrayToOutputFormat($locations)
        ], Response::HTTP_OK);
    }

    /**
     * @Route("locations", name="create_location", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $locationDto = new LocationDto(json_decode($request->getContent(), true));

        $shelf = $this->shelfRepository->find($locationDto->shelfId);

        if (!$shelf) {
            return $this->json([
                'shelf' => sprintf("Shelf with id %s not found", $locationDto->shelfId)
            ], Response::HTTP_NOT_FOUND);
        }

        try {
            $barcode = $this->barcodeCounterRepository->generateAndIncrementMsiBarcode();
            $location = $this->locationRepository->createLocationOnShelf($locationDto, $shelf, $barcode);
        } catch (Exception $exception) {
            return $this->json([
                'status' => 'error',
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json($this->locationAdaptor->toOutputFormat($location), Response::HTTP_OK);
    }

    /**
     * @Route("locations/{locationId}", name="show_location", methods={"GET"})
     * @param $locationId
     * @return JsonResponse
     */
    public function show($locationId)
    {
        $location = $this->locationRepository->find($locationId);

        if (!$location) {
            return $this->json([
                'location' => sprintf("Location with id %s not found", $locationId)
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->json($this->locationAdaptor->toOutputFormat($location), Response::HTTP_OK);
    }

    /**
     * @Route("locations/{locationId}", name="delete_location", methods={"DELETE"})
     * @param $locationId
     * @return JsonResponse
     */
    public function delete($locationId)
    {
        $location = $this->locationRepository->find($locationId);

        if (!$location) {
            return $this->json([
                'location' => sprintf("Location with id %s not found", $locationId)
            ], Response::HTTP_NOT_FOUND);
        }

        try {
            $this->locationRepository->deleteLocation($location);
        } catch (Exception $exception) {
            return $this->json([
                'status' => 'error',
                'message' => $exception->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/available-locations", name="avaialable_locations", methods={"GET"})
     */
    public function getAvailableLocations(Request $request)
    {
        $productsIds = $request->query->has('products') && !is_array($request->query->get('products')) ? explode(',', $request->query->get('products')) : [];

        if (!count($productsIds)) {
            return $this->json([
                'status' => 'error',
                'message' => 'Please provide products ids as following: products=1,2,3'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $response = $this->productsClient->getProductsByIds($productsIds);

        if ($response->getStatusCode() != 200) {
            return new JsonResponse($response->getBody()->getContents(), $response->getStatusCode(), [], true);
        }

        $products = json_decode($response->getBody()->getContents(), true);

        $locations = [];

        foreach ($products as $product) {
            $locations[$product['id']] = $this->getAvailableLocationsForProduct($product);
        }

        return $this->json($locations, Response::HTTP_OK);

    }

    private function getAvailableLocationsForProduct(array $product): array
    {
        $volume = ($product['dimensions']['height'] * $product['dimensions']['length'] * $product['dimensions']['width']);

        $locations = $this->locationRepository->getAvailableLocationThatFits($volume);

        if (count($locations) == 0) {
            return [];
        }

        $availableLocations = [];

        /** @var Location $location */
        foreach ($locations as $location) {
            if ($product['dimensions']['weight'] > $location->getMaxWeightAdmitted()) {
                continue;
            }

            $availableLocations[] = [
                'id' => $location->getId(),
                'barcode' => $location->getBarcode(),
                'width' => $location->getWidth(),
                'height' => $location->getHeight(),
                'length' => $location->getLength(),
                'max_weight_admitted' => $location->getMaxWeightAdmitted()
            ];
        }

        return $availableLocations;
    }
}
