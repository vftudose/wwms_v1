<?php

namespace App\DataFixtures;

use App\Entity\BarcodeCounter;
use App\Entity\Location;
use App\Entity\Shelf;
use App\Entity\Zone;
use App\Repository\BarcodeCounterRepository;
use App\Repository\ShelfRepository;
use App\Repository\ZoneRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private $zoneRepository;
    private $shelfRepository;
    private $barcodeCounterRepository;

    public function __construct(
        ZoneRepository $zoneRepository,
        ShelfRepository $shelfRepository,
        BarcodeCounterRepository $barcodeCounterRepository
    )
    {
        $this->zoneRepository = $zoneRepository;
        $this->shelfRepository = $shelfRepository;
        $this->barcodeCounterRepository = $barcodeCounterRepository;
    }

    public function load(ObjectManager $manager)
    {
        $this->createMsiBarcodeCounter($manager);
        $this->createWarehouseZones($manager);
        $this->createShelvesForPickingZone($manager);
        $this->createShelvesForStorageZone($manager);
        $this->createLocationsForPickingShelves($manager);

        $manager->flush();
    }

    private function createWarehouseZones(ObjectManager $manager)
    {
        $zones = [
            [
                'name' => 'Zona Depozitare 1',
                'type' => Zone::TYPE_STORAGE
            ],
            [
                'name' => 'Zona Picking 1',
                'type' => Zone::TYPE_PICKING
            ],
            [
                'name' => 'Zona Impachetare 1',
                'type' => Zone::TYPE_PACKAGE
            ],
            [
                'name' => 'Zona Exit 1',
                'type' => Zone::TYPE_EXIT
            ],
        ];

        foreach ($zones as $zoneInfo) {

            $zone = new Zone();
            $zone->setName($zoneInfo['name'])
                ->setType($zoneInfo['type'])
                ->setCreatedAt(new \DateTime())
                ->setUpdatedAt(new \DateTime());

            $manager->persist($zone);
        }

        $manager->flush();
    }

    private function createShelvesForPickingZone(ObjectManager $manager)
    {
        $zone = $this->zoneRepository->findOneBy(['name' => 'Zona Picking 1']);

        foreach ([1, 2, 3] as $index) {
            $shelf = new Shelf();
            $shelf->setZone($zone)
                ->setLength(1000)
                ->setHeight(200)
                ->setWidth(100)
                ->setUpdatedAt(new \DateTime())
                ->setCreatedAt(new \DateTime());
            $manager->persist($shelf);
        }

        $manager->flush();
    }

    private function createShelvesForStorageZone(ObjectManager $manager)
    {
        $zone = $this->zoneRepository->findOneBy(['name' => 'Zona Depozitare 1']);

        foreach ([1, 2] as $index) {
            $shelf = new Shelf();
            $shelf->setZone($zone)
                ->setLength(1000)
                ->setHeight(1000)
                ->setWidth(100)
                ->setUpdatedAt(new \DateTime())
                ->setCreatedAt(new \DateTime());
            $manager->persist($shelf);
        }

        $manager->flush();
    }

    private function createLocationsForPickingShelves(ObjectManager $manager)
    {
        $shelves = $this->shelfRepository->findAll();
        foreach ($shelves as $shelf) {
            for ($i = 0; $i < 20; $i++) {
                $barcode = $this->barcodeCounterRepository->generateAndIncrementMsiBarcode();

                $location = new Location();
                $location->setWidth(100)
                    ->setHeight(100)
                    ->setLength(100)
                    ->setType(Location::TYPE_FIXED)
                    ->setBarcode($barcode)
                    ->setShelf($shelf)
                    ->setCreatedAt(new \DateTime())
                    ->setUpdatedAt(new \DateTime());

                $manager->persist($location);
            }
        }

        $manager->flush();
    }

    private function createMsiBarcodeCounter(ObjectManager $manager)
    {
        $counter = new BarcodeCounter();
        $counter->setName(BarcodeCounter::MSI);
        $counter->initCounter();
        $manager->persist($counter);
        $manager->flush();
    }
}
