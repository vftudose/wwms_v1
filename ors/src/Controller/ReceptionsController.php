<?php

namespace App\Controller;

use App\Entity\Reception;
use App\Repository\ReceptionRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Craft\Dto\Reception as ReceptionDto;

class ReceptionsController extends AbstractController
{
    private $receptionRepository;

    public function __construct(ReceptionRepository $receptionRepository)
    {
        $this->receptionRepository = $receptionRepository;
    }

    /**
     * @Route("receptions", name="create_reception", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $receptionDto = new ReceptionDto(json_decode($request->getContent(), true));

        try {
            /** @var Reception $reception */
            $reception = $this->receptionRepository->create($receptionDto);
        } catch (Exception $exception) {
            return $this->json([
                'reception' => $exception->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json([
            'id' => $reception->getId(),
            'name' => $reception->getName(),
            'status' => $reception->getStatus(),
            'products' => $reception->getReceptionLinesAsArray(),
            'created_at' => $reception->getCreatedAt()
        ], Response::HTTP_OK);
    }

    /**
     * @Route("receptions/{receptionId}", name="show_reception", methods={"GET"})
     * @param $receptionId
     * @return JsonResponse
     */
    public function show($receptionId)
    {
        $reception = $this->receptionRepository->find($receptionId);

        if (!$reception) {
            return $this->json([
                'order' => sprintf('Reception with id: %s not found!', $receptionId)
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->json([
            'id' => $reception->getId(),
            'name' => $reception->getName(),
            'status' => $reception->getStatus(),
            'products' => $reception->getReceptionLinesAsArray(),
            'created_at' => $reception->getCreatedAt()
        ], Response::HTTP_OK);
    }

    /**
     * @Route("receptions", name="all_receptions", methods={"GET"})
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;
        $receptions = $this->receptionRepository->getReceptionsPaginated($page);
        $total = $this->receptionRepository->count([]);

        $processedReceptions = [];
        /** @var Reception $reception */
        foreach ($receptions as $reception) {
            $processedReceptions[] = [
                'id' => $reception->getId(),
                'name' => $reception->getName(),
                'status' => $reception->getStatus(),
                'products' => $reception->getReceptionLinesAsArray(),
                'created_at' => $reception->getCreatedAt()
            ];
        }

        $data = [
            'total' => $total,
            'page' => $page,
            'per_page' => ReceptionRepository::PER_PAGE,
            'data' => $processedReceptions
        ];

        return $this->json($data, Response::HTTP_OK);
    }
}