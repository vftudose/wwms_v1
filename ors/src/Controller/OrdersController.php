<?php

namespace App\Controller;

use App\Entity\Order;
use Exception;
use App\Repository\OrderRepository;
use Craft\Dto\Order\Order as OrderDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrdersController extends AbstractController
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @Route("orders", name="all_orders", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;
        $orders = $this->orderRepository->getOrdersPaginated($page);
        $total = $this->orderRepository->count([]);

        $processedOrders = [];
        /** @var Order $order */
        foreach ($orders as $order) {
            $processedOrders[] = [
                'id' => $order->getId(),
                'name' => $order->getName(),
                'status' => $order->getStatus(),
                'products' => $order->getOrderLinesAsArray(),
                'created_at' => $order->getCreatedAt()
            ];
        }

        $data = [
            'total' => $total,
            'page' => $page,
            'per_page' => OrderRepository::PER_PAGE,
            'data' => $processedOrders
        ];

        return $this->json($data, Response::HTTP_OK);
    }


    /**
     * @Route("orders", name="create_order", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $orderDto = new OrderDto(json_decode($request->getContent(), true));

        try {
            $order = $this->orderRepository->create($orderDto);
        } catch (Exception $exception) {
            return $this->json([
                'order' => $exception->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json([
            'id' => $order->getId(),
            'name' => $order->getName(),
            'status' => $order->getStatus(),
            'products' => $order->getOrderLinesAsArray(),
            'created_at' => $order->getCreatedAt()
        ], Response::HTTP_OK);
    }

    /**
     * @Route("orders/{orderId}", name="show_order", methods={"GET"})
     * @param $orderId
     * @return JsonResponse
     */
    public function show($orderId)
    {
        $order = $this->orderRepository->find($orderId);

        if (!$order) {
            return $this->json([
                'order' => sprintf('Order with id: %s not found!', $orderId)
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->json([
            'id' => $order->getId(),
            'name' => $order->getName(),
            'status' => $order->getStatus(),
            'products' => $order->getOrderLinesAsArray(),
            'created_at' => $order->getCreatedAt()
        ], Response::HTTP_OK);
    }
}
