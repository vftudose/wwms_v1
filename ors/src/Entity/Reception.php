<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReceptionRepository")
 */
class Reception
{
    const STATUS_PENDING = 'pending';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_FINISHED = 'finished';
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="string")
     */
    private $documentName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReceptionLine", mappedBy="reception", orphanRemoval=true, cascade={"persist"})
     */
    private $receptionLines;

    public function __construct()
    {
        $this->receptionLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentName(): ?string
    {
        return $this->documentName;
    }

    public function setDocumentName(string $documentName = null): self
    {
        $this->documentName = $documentName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|ReceptionLine[]
     */
    public function getReceptionLines(): Collection
    {
        return $this->receptionLines;
    }

    public function addReceptionLine(ReceptionLine $receptionLine): self
    {
        if (!$this->receptionLines->contains($receptionLine)) {
            $this->receptionLines[] = $receptionLine;
            $receptionLine->setReception($this);
        }

        return $this;
    }

    public function removeReceptionLine(ReceptionLine $receptionLine): self
    {
        if ($this->receptionLines->contains($receptionLine)) {
            $this->receptionLines->removeElement($receptionLine);
            // set the owning side to null (unless already changed)
            if ($receptionLine->getReception() === $this) {
                $receptionLine->setReception(null);
            }
        }

        return $this;
    }

    public function getReceptionLinesAsArray()
    {
        $lines = [];

        /** @var ReceptionLine $line */
        foreach ($this->getReceptionLines() as $line) {
            $lines[] = [
                'id' => $line->getId(),
                'product-id' => $line->getProductId(),
                'expected-quantity' => $line->getExpectedQuantity(),
                'received-quantity' => $line->getReceivedQuantity()
            ];
        }

        return $lines;
    }

    public function addReceptionLines(Collection $receptionLines): self
    {
        foreach ($receptionLines as $receptionLine) {
            $this->addReceptionLine($receptionLine);
        }

        return $this;
    }
}
