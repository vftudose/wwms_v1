<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderLineRepository")
 */
class OrderLine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $productId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="orderLines")
     * @ORM\JoinColumn(nullable=false)
     */
    private $order;

    /**
     * @ORM\Column(type="integer", options={"default": 1})
     */
    private $quantity;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderLineTask", mappedBy="orderLine", orphanRemoval=true)
     */
    private $orderLineTasks;

    public function __construct()
    {
        $this->orderLineTasks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function getProductId(): ?int
    {
        return $this->productId;
    }

    public function setProductId(int $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function setQuantity($quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|OrderLineTask[]
     */
    public function getOrderLineTasks(): Collection
    {
        return $this->orderLineTasks;
    }

    public function addOrderLineTask(OrderLineTask $orderLineTask): self
    {
        if (!$this->orderLineTasks->contains($orderLineTask)) {
            $this->orderLineTasks[] = $orderLineTask;
            $orderLineTask->setOrderLine($this);
        }

        return $this;
    }

    public function removeOrderLineTask(OrderLineTask $orderLineTask): self
    {
        if ($this->orderLineTasks->contains($orderLineTask)) {
            $this->orderLineTasks->removeElement($orderLineTask);
            // set the owning side to null (unless already changed)
            if ($orderLineTask->getOrderLine() === $this) {
                $orderLineTask->setOrderLine(null);
            }
        }

        return $this;
    }
}
