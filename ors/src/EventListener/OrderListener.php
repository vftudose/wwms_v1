<?php

namespace App\EventListener;

use App\Repository\OrderLineRepository;
use App\Repository\OrderRepository;
use Craft\Dto\Order\OrderLine as OrderLineDto;
use Craft\Event\OrderReservedEvent;
use Craft\Event\OrderTasksGeneratedEvent;
use Craft\Event\OrderUnreservedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class OrderListener
{
    private $orderRepository;
    private $orderLineRepository;
    private $dispatcher;
    private $manager;
    private $logger;

    public function __construct(
        OrderRepository $orderRepository,
        OrderLineRepository $orderLineRepository,
        EventDispatcherInterface $dispatcher,
        EntityManagerInterface $manager,
        LoggerInterface $logger
    )
    {
        $this->orderRepository = $orderRepository;
        $this->orderLineRepository = $orderLineRepository;
        $this->dispatcher = $dispatcher;
        $this->manager = $manager;
        $this->logger = $logger;
    }

    public function onUnreservedOrder(OrderUnreservedEvent $event)
    {
        $orderDto = $event->getOrder();

        $order = $this->orderRepository->find($orderDto->id);

        $order->toUnreserved();

        $this->manager->persist($order);
        $this->manager->flush();

    }

    public function onReservedOrder(OrderReservedEvent $event)
    {
        $orderDto = $event->getOrder();

        $order = $this->orderRepository->find($orderDto->id);

        $order->toReserved();

        $this->manager->persist($order);
        $this->manager->flush();
    }

    public function onOrderTasksGenerated(OrderTasksGeneratedEvent $event)
    {
        $orderDto = $event->getOrder();

        /** @var OrderLineDto $orderLineDto */
        foreach ($orderDto->products as $orderLineDto) {
            $orderLine = $this->orderLineRepository->find($orderLineDto->id);

            if (!$orderLine) {
                throw new Exception(sprintf('order line with id %s not found', $orderLineDto->id));
            }

            $this->orderRepository->createOrderLineTasks($orderLine, $orderLineDto);
        }
    }
}
