<?php

namespace App\Repository;

use App\Entity\ReceptionLine;
use App\Entity\Reception;
use Craft\Dto\Reception as ReceptionDto;
use Craft\Dto\ReceptionLine as ReceptionLineDto;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @method Reception|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reception|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reception[]    findAll()
 * @method Reception[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReceptionRepository extends ServiceEntityRepository
{
    const PER_PAGE = 100;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Reception::class);
    }

    public function create(ReceptionDto $receptionDto)
    {
        /** @var EntityManager $em */
        $em = $this->getEntityManager();

        $timestamp = new DateTime();

        $em->beginTransaction();

        try {
            $reception = new Reception();

            $reception->setName($receptionDto->name)
                ->setStatus(Reception::STATUS_PENDING)
                ->setUpdatedAt($timestamp)
                ->setCreatedAt($timestamp)
                ->setDocumentName('none');
                //TODO attach documents to receptions
            $reception->addReceptionLines($this->createReceptionLines($receptionDto));

            $em->persist($reception);
            $em->flush();
            $em->getConnection()->commit();

            return $reception;
        } catch (Exception $exception) {
            $em->rollback();
            throw $exception;
        }
    }

    private function createReceptionLines(ReceptionDto $receptionDto): ArrayCollection
    {
        $timestamp = new DateTime();

        $collection = new ArrayCollection();

        /** @var ReceptionLineDto $receptionLineDto */
        foreach ($receptionDto->products as $receptionLineDto) {
            $receptionLine = new ReceptionLine();
            $receptionLine->setProductId($receptionLineDto->productId)
                ->setExpectedQuantity($receptionLineDto->expectedQuantity)
                ->setReceivedQuantity(0)
                ->setUpdatedAt($timestamp)
                ->setCreatedAt($timestamp);

            $collection->add($receptionLine);
        }

        return $collection;
    }

    public function getReceptionsPaginated(int $page)
    {
        return $this->createQueryBuilder('r')
            ->join(ReceptionLine::class, 'rl', Join::ON)
            ->getQuery()
            ->setFirstResult(self::PER_PAGE * ($page - 1))
            ->setMaxResults(self::PER_PAGE)
            ->getResult();
    }
}
