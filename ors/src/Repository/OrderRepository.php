<?php

namespace App\Repository;

use App\Entity\Order;
use App\Entity\OrderLine;
use App\Entity\OrderLineTask;
use Craft\Dto\Order\Order as OrderDto;
use Craft\Dto\Order\OrderLine as OrderLineDto;
use Craft\Dto\Order\OrderLineTask as OrderLineTaskDto;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    const PER_PAGE = 100;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @param OrderDto $orderDto
     * @return Order
     * @throws Exception
     */
    public function create(OrderDto $orderDto)
    {
        /** @var EntityManager $em */
        $em = $this->getEntityManager();

        $timestamp = new DateTime();

        $em->beginTransaction();

        try {
            $order = new Order();
            $order->setName($orderDto->name)
                ->setUpdatedAt($timestamp)
                ->setCreatedAt($timestamp);

            $order->addOrderLines($this->createOrderLines($orderDto));

            $em->persist($order);
            $em->flush();
            $em->getConnection()->commit();
        } catch (Exception $exception) {
            $em->rollback();
            throw $exception;
        }

        return $order;
    }

    private function createOrderLines(OrderDto $orderDto): ArrayCollection
    {
        $timestamp = new DateTime();

        $collection = new ArrayCollection();

        /** @var OrderLineDto $orderLineDto */
        foreach ($orderDto->products as $orderLineDto) {
            $orderLine = new OrderLine();
            $orderLine->setProductId($orderLineDto->productId)
                ->setQuantity($orderLineDto->quantity)
                ->setUpdatedAt($timestamp)
                ->setCreatedAt($timestamp);

            $collection->add($orderLine);
        }

        return $collection;
    }

    public function getOrdersPaginated(int $page)
    {
        return $this->createQueryBuilder('o')
            ->join(OrderLine::class, 'ol', Join::ON)
            ->getQuery()
            ->setFirstResult(self::PER_PAGE * ($page - 1))
            ->setMaxResults(self::PER_PAGE)
            ->getResult();
    }

    public function createOrderLineTasks(OrderLine $orderLine, OrderLineDto $orderLineDto)
    {
        /** @var OrderLineTaskDto $task */
        foreach ($orderLineDto->tasks as $task) {

            $orderLineTask = new OrderLineTask();
            $orderLineTask->setOrderLine($orderLine)
                ->setStatus($task->status)
                ->setUpdatedAt(new DateTime())
                ->setCreatedAt(new DateTime())
                ->setTaskId($task->taskId);

            $this->_em->persist($orderLineTask);

        }

        $this->_em->flush();
    }
}
