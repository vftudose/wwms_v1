import { takeLatest, put, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import request, { getBaseUrl } from '../../utils/request';
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL } from './actionTypes';
import { loginRequest, loginSuccess, loginFail } from './actions';

function* loginSaga(data) {
  try {
    const params = requestInit({
      method: 'GET',
      body: JSON.stringify(...data),
    });
    const response = yield call(request, `${getBaseUrl()}/`, params);
    yield put(loginSuccess(response));
  } catch (error) {
    yield put(loginFail(error));
  }
}

export default function* root() {
  yield takeLatest(LOGIN_REQUEST, loginSaga);
}
