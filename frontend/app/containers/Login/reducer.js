import { fromJS } from 'immutable';
import { LOGIN_SUCCESS, LOGIN_FAIL } from './constants';

export const initialState = fromJS({
  token: '',
});

function loginReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return { ...state, token: action.token };
    case LOGIN_FAIL:
      return { ...state, token: '' };
    default:
      return state;
  }
}

export default loginReducer;
