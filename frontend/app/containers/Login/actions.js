import { LOGIN_REQUEST, LOGIN_FAIL, LOGIN_SUCCESS } from './constants';

export function loginRequest(loginData) {
  return {
    type: LOGIN_REQUEST,
    loginData,
  };
}

export function loginSuccess() {
  return {
    type: LOGIN_SUCCESS,
    token,
  };
}

export function loginFail(error) {
  return {
    type: LOGIN_FAIL,
    error,
  };
}
