export default class CustomError extends Error {
	constructor(params) {
		let name = 'CustomError';
		let message = params;
		let code;
		let response;

		if (typeof params === 'object') {
			name = params.name;
			message = params.message;
			code = params.code;
			response = params.response;
		}

		super(message);

		this.name = name;
		this.code = code;
		this.response = response;
	}
}
