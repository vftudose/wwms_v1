import 'whatwg-fetch';
import merge from 'lodash/merge';
import cookie from 'react-cookies';

import CustomError from 'utils/customError';

class RequestError extends CustomError {
	constructor(params) {
		super({ name: 'RequestError', ...params });
	}
}

export const defaultPaths = {
	dev: 'localhost:8000',
	prod: ''
};

function getDefaultOptions() {
	return {
		mode: 'cors',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			token: getToken()
		}
	};
}

export function getToken() {
	return location.hostname === 'localhost' || location.hostname === '127.0.0.1'
		? ''
		: cookie.load('token');
}

export function getBaseUrl() {
	if (location.hostname === 'localhost' || location.hostname === '127.0.0.1') {
		return defaultPaths.dev || '';
	}
	return `${location.pathname.split('/')[0]}`;
}

function parseResponse(response) {
	if (response.status === 401 || response.status === 403) {
		window.location.href = `${getBaseUrl()}/`;
	}

	return response.json().then(result => {
		if (response.status === 204 || response.status === 205) {
			return null;
		}

		if (
			response.status >= 200 &&
			response.status < 300 &&
			result.code !== '500'
		) {
			return result;
		}

		throw new RequestError({ message: result.Message, response: result });
	});
}

export default function request(url, options) {
	return fetch(url, merge({}, getDefaultOptions(), options)).then(
		parseResponse
	);
}
