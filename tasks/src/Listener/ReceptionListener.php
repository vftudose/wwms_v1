<?php

namespace App\Listener;

use App\Entity\Task;
use App\Repository\TaskRepository;
use Craft\Client\ProductsClient;
use Craft\Dto\ReceptionLine as ReceptionLineDto;
use Craft\Dto\ReceptionLineTask as ReceptionLineTaskDto;
use Craft\Client\StockClient;
use Craft\Dto\Reception as ReceptionDto;
use Craft\Event\ReceptionCreatedEvent;
use Craft\Event\ReceptionTasksGeneratedEvent;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ReceptionListener
{
    private $dispatcher;
    private $logger;
    private $taskRepository;
    private $stockClient;
    private $productsClient;

    public function __construct(
        TaskRepository $taskRepository,
        EventDispatcherInterface $dispatcher,
        LoggerInterface $logger
    )
    {
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
        $this->taskRepository = $taskRepository;
        $this->productsClient = new ProductsClient($_ENV['PRODUCTS_HOST']);
        $this->stockClient = new StockClient($_ENV['STOCK_HOST']);
    }

    public function onReceptionCreated(ReceptionCreatedEvent $event)
    {
        $data = $event->getData();

        $productIds = $event->getReception()->getProductsIds();

        $productsResponse = $this->productsClient->getProductsByIds($productIds);
        $stockResponse = $this->stockClient->getAvailableLocations($productIds);

        if ($productsResponse->getStatusCode() != Response::HTTP_OK) {
            $this->logger->error($productsResponse->getBody()->getContents());
        }

        if ($stockResponse->getStatusCode() != Response::HTTP_OK) {
            $this->logger->error($stockResponse->getBody()->getContents());
        }

        $products = $this->indexProductsById(json_decode($productsResponse->getBody()->getContents(), true));

        $groupedLocations = $this->decrementLocationsDimensionsFromInProgressTasks(json_decode($stockResponse->getBody()->getContents(), true), $products);

        $this->taskRepository->beginTransaction();
        $tasks = [];
        try {
            $locationIndex = 0;
            foreach ($data['products'] as $receptionLine) {
                $location = $groupedLocations[$receptionLine['product-id']][$locationIndex];
                for ($i = 0; $i < $receptionLine['expected-quantity']; $i++) {
                    if (!isset($groupedLocations[$receptionLine['product-id']][$locationIndex])) {
                        throw new Exception('No more locations founded!');
                    }

                    $product = $products[$receptionLine['product-id']];

                    if ($this->productCanFitInLocation($product, $location)) {
                        $tasks[$receptionLine['id']][] = $this->taskRepository->generateReplenishmentTask($data['id'], $receptionLine['product-id'], $location['id']);

                        $this->decrementProductDimensionsFromLocation($product, $location);
                    } else {
                        $locationIndex++;
                        $location = $groupedLocations[$receptionLine['product-id']][$locationIndex];
                        $this->taskRepository->generateReplenishmentTask($data['id'], $receptionLine['product-id'], $location['id']);
                        $this->decrementProductDimensionsFromLocation($product, $location);
                    }
                }
            }

            $this->taskRepository->commit();
        } catch (Exception $exception) {
            $this->taskRepository->rollback();
            $this->logger->error($exception->getMessage());
            throw $exception;
        }

        $receptionDto = $this->updateOrderDtoWithTasks($tasks, $event->getReception());


        $this->dispatcher->dispatch(new ReceptionTasksGeneratedEvent($receptionDto), ReceptionTasksGeneratedEvent::EVENT_NAME);
    }

    private function productCanFitInLocation($product, $location)
    {
        if ($location['width'] - $product['dimensions']['width'] < 0) {
            return false;
        }

        if ($location['height'] - $product['dimensions']['height'] < 0) {
            return false;
        }

        if ($location['length'] - $product['dimensions']['length'] < 0) {
            return false;
        }

        if ($location['max_weight_admitted'] - $product['dimensions']['weight'] < 0) {
            return false;
        }

        return true;
    }

    private function decrementProductDimensionsFromLocation($product, &$location)
    {
        $location['width'] -= $product['dimensions']['width'];
        $location['height'] -= $product['dimensions']['height'];
        $location['length'] -= $product['dimensions']['length'];
        $location['max_weight_admitted'] -= $product['dimensions']['weight'];
    }
    private function updateOrderDtoWithTasks(array $groupedTasks, ReceptionDto $receptionDto)
    {
        //TODO REMOVE THIS FROM HERE ...
        $receptionLineTasks = [];

        /** @var Task $task */
        foreach ($groupedTasks as $receptionLineId => $lineTasks) {
            foreach ($lineTasks as $task) {
                $receptionLineTasks[$receptionLineId][] = new ReceptionLineTaskDto([
                    'task-id' => $task->getId(),
                    'type' => $task->getType(),
                    'location-id' => $task->getLocationId(),
                    'status' => $task->getStatus(),
                    'product-id' => $task->getProductId()
                ]);
            }
        }

        /** @var ReceptionLineDto $orderLine */
        foreach ($receptionDto->products as $receptionLine) {
            $receptionLine->tasks = $receptionLineTasks[$receptionLine->id];
        }

        return $receptionDto;
    }

    private function indexProductsById(array $products)
    {
        return array_column($products, null, 'id');
    }

    private function decrementLocationsDimensionsFromInProgressTasks($groupedLocations, array $products)
    {
        foreach ($groupedLocations as $key => $availableLocations) {
            foreach ($availableLocations as $locationIndex => $location) {
                $tasks = $this->taskRepository->findInProgressTasksByLocationIds([$location['id']], Task::TYPE_REPLENISHMENT);
                foreach ($tasks as $task) {
                    $groupedLocations[$key][$locationIndex]['width'] -= $products[$task->getProductId()]['dimensions']['width'];
                    $groupedLocations[$key][$locationIndex]['height'] -= $products[$task->getProductId()]['dimensions']['height'];
                    $groupedLocations[$key][$locationIndex]['length'] -= $products[$task->getProductId()]['dimensions']['length'];
                    $groupedLocations[$key][$locationIndex]['max_weight_admitted'] -= $products[$task->getProductId()]['dimensions']['weight'];
                }
            }
        }

        return $groupedLocations;
    }
}