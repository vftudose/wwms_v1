<?php

namespace App\Listener;

use App\Entity\Task;
use App\Repository\TaskRepository;
use Craft\Dto\Order\Order as OrderDto;
use Craft\Dto\Order\OrderLine as OrderLineDto;
use Craft\Dto\Order\OrderLineTask as OrderLineTaskDto;
use Craft\Event\OrderReservedEvent;
use Craft\Event\OrderTasksGeneratedEvent;
use Craft\Event\PublishableEvent;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class OrderListener
{
    private $dispatcher;
    private $logger;
    private $taskRepository;

    public function __construct(
        TaskRepository $taskRepository,
        EventDispatcherInterface $dispatcher,
        LoggerInterface $logger
    )
    {
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
        $this->taskRepository = $taskRepository;
    }

    public function onReservedOrder(OrderReservedEvent $event): bool
    {
        $order = $event->getOrder();

        try {
            $groupedTasks = $this->taskRepository->generatePickingTasksForOrder($order);

            $order = $this->updateOrderDtoWithTasks($groupedTasks, $order);

        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
            return false;
        }

        $this->dispatcher->dispatch(new OrderTasksGeneratedEvent($order), PublishableEvent::EVENT_NAME);
        return true;
    }

    private function updateOrderDtoWithTasks(array $groupedTasks, OrderDto $order)
    {
        //TODO REMOVE THIS FROM HERE ...
        $orderLineTasks = [];

        /** @var Task $task */
        foreach ($groupedTasks as $orderLineId => $lineTasks) {
            foreach ($lineTasks as $task) {
                $orderLineTasks[$orderLineId][] = new OrderLineTaskDto([
                    'task-id' => $task->getId(),
                    'type' => $task->getType(),
                    'location-id' => $task->getLocationId(),
                    'status' => $task->getStatus(),
                    'product-id' => $task->getProductId()
                ]);
            }
        }

        /** @var OrderLineDto $orderLine */
        foreach ($order->products as $orderLine) {
            $orderLine->tasks = $orderLineTasks[$orderLine->id];
        }

        return $order;
    }
}