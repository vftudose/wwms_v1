<?php

namespace App\Controller;

use App\Form\OrderPicking;
use App\Form\ScanContainerStep;
use App\Form\ScanLocationType;
use App\Form\Steps;
use App\Repository\TaskRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PdaController extends AbstractController
{
    /**
     * @Route(name="login", methods={"GET"}, path="pda/login")
     */
    public function login(Request $request)
    {
        return $this->render('login.html.twig', [
            'auth_action' => '/api/v1/pda/authenticate',
            'error' => $request->query->get('error')
        ]);
    }

    /**
     * @Route(name="operations", methods={"GET"}, path="pda/operations")
     */
    public function showOperationsList(Request $request)
    {
        return $this->render('operations.html.twig', [
            'operations' => [
                'picking',
                'replenishment'
            ]
        ]);
    }

    /**
     * @Route(name="picking", methods={"GET"}, path="pda/operations/picking")
     */
    public function showPickingOperationsList(Request $request)
    {
        return $this->render('picking-operations.html.twig', [
            'operations' => [
                'order-picking',
            ],
        ]);
    }

    /**
     * @Route(name="replenishment", methods={"GET"}, path="pda/operations/replenishment")
     */
    public function showReplenishmentOperationsList(Request $request)
    {
        return $this->render('replenishment-operations.html.twig', [
            'operations' => []
        ]);
    }

    /**
     * @Route(name="test", methods={"GET"}, path="pda/test")
     */
    public function test(Request $request)
    {
        $form = $this->createForm(ScanLocationType::class);

        return $this->render('scan-location.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
