<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\ScanContainerType;
use App\Form\ScanLocationType;
use App\Form\ScanProductType;
use App\Repository\TaskRepository;
use Craft\Client\OrsClient;
use Craft\Client\ProductsClient;
use Craft\Client\StockClient;
use Craft\Event\PublishableEvent;
use Craft\Event\TaskPickedEvent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderPickingController extends AbstractController
{
    const TITLE = 'ORDER PICKING';

    private $stockClient;
    private $productClient;
    private $taskRepository;
    private $eventDispatcher;
    private $orsClient;

    public function __construct(TaskRepository $taskRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->taskRepository = $taskRepository;
        $this->stockClient = new StockClient($_ENV['STOCK_HOST']);
        $this->productClient = new ProductsClient($_ENV['PRODUCTS_HOST']);
        $this->orsClient = new OrsClient($_ENV['ORS_HOST']);
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route(name="scan-location", methods={"GET","POST"}, path="pda/order-picking/scan-location")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function scanLocation(Request $request)
    {
        //TODO replace with request user id
        $userId = '5d17652d59e3b40ed85a8b5f';

        $task = $this->taskRepository->findUserInProgressTask($userId);

        if ($task == null) {
            $task = $this->taskRepository->findUnassignedTask();
        }

        if ($task) {
            $this->taskRepository->assignAllTasksForOrder($userId, $task->getOrderId());
        }

        $form = $this->createForm(ScanLocationType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (!$this->doesLocationExists($form->get('location-barcode')->getData())) {
                return $this->redirectToRoute('scan-location');
            }

            $product = $this->getTaskProduct($task);

            return $this->redirectToRoute('scan-product', [
                'location-barcode' => $form->get('location-barcode')->getData(),
                'product-name' => $product['name'],
                'order-id' => $task->getOrderId(),
                'task-id' => $task->getId()
            ]);
        }

        $response = $this->stockClient->getLocation($task->getLocationId());

        $location = json_decode($response->getBody()->getContents(), true);

        return $this->render('pda/scan-location.html.twig', [
            'title' => self::TITLE,
            'form' => $form->createView(),
            'location' => $location
        ]);
    }

    /**
     * @Route(name="scan-product" , methods={"GET", "POST"}, path="pda/order-picking/scan-product")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function scanProduct(Request $request)
    {
        $form = $this->createForm(ScanProductType::class);

        $form->get('location-barcode')->setData($request->query->get('location-barcode'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (!$this->doesProductExists($form->get('product-barcode')->getData())) {
                return $this->redirectToRoute('scan-product', [
                    'location-barcode' => $form->get('location-barcode')->getData(),
                    'product-name' => $request->query->get('product-name'),
                    'order-id' => $request->query->get('order-id'),
                    'task-id' => $request->query->get('task-id')
                ]);
            }

            return $this->redirectToRoute('scan-container', [
                'location-barcode' => $request->query->get('location-barcode'),
                'product-barcode' => $form->get('product-barcode')->getData(),
                'order-id' => $request->query->get('order-id'),
                'task-id' => $request->query->get('task-id')
            ]);
        }

        return $this->render('pda/scan-product.html.twig', [
            'title' => self::TITLE,
            'form' => $form->createView(),
            'product_name' => $request->query->get('product-name')
        ]);
    }

    /**
     * @Route(name="scan-container" , methods={"GET", "POST"}, path="pda/order-picking/scan-container")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function scanContainer(Request $request)
    {
        $form = $this->createForm(ScanContainerType::class);

        $locationBarcode = $request->query->get('location-barcode');
        $productBarcode = $request->query->get('product-barcode');

        $form->get('location-barcode')->setData($locationBarcode);
        $form->get('product-barcode')->setData($productBarcode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $containerBarcode = $form->get('container-barcode')->getData();

            $response = $this->stockClient->getAllContainers(1, $containerBarcode);

            if ($response->getStatusCode() != Response::HTTP_OK) {
                return $this->redirectToRoute('scan-container', [
                    'location-barcode' => $locationBarcode,
                    'product-barcode' => $productBarcode,
                    'task-id' => $request->query->get('task-id')
                ]);
            }

            $containers = json_decode($response->getBody()->getContents(), true);

            if (empty($containers['data'])) {
                return $this->redirectToRoute('scan-container', [
                    'location-barcode' => $locationBarcode,
                    'product-barcode' => $productBarcode,
                    'task-id' => $request->query->get('task-id'),
                    'error' => 'Container does not exists'
                ]);
            }

            $response = $this->stockClient->getContainerStockLines($containers['data'][0]['id']);

            if ($response->getStatusCode() != Response::HTTP_OK) {
                return $this->redirectToRoute('scan-container', [
                    'location-barcode' => $locationBarcode,
                    'product-barcode' => $productBarcode,
                    'task-id' => $request->query->get('task-id'),
                    'error' => 'Error while fetching stock lines'
                ]);
            }

            $stocks = json_decode($response->getBody()->getContents(), true);

            $containerProductsIds = array_column($stocks, 'product-id');

            $response = $this->orsClient->getOrder($request->query->get('order-id'));

            if ($response->getStatusCode() != Response::HTTP_OK) {
                return $this->redirectToRoute('scan-container', [
                    'location-barcode' => $locationBarcode,
                    'product-barcode' => $productBarcode,
                    'task-id' => $request->query->get('task-id'),
                    'order-id' => $request->query->get('order-id')
                ]);
            }

            $order = json_decode($response->getBody()->getContents(), true);

            $orderProductsIds = array_unique(array_column($order['products'],'product-id'));

            foreach ($containerProductsIds as $containerProductId) {
                if (!in_array($containerProductId, $orderProductsIds)) {
                    return $this->redirectToRoute('scan-container', [
                        'location-barcode' => $locationBarcode,
                        'product-barcode' => $productBarcode,
                        'order-id' => $request->query->get('order-id'),
                        'task-id' => $request->query->get('task-id'),
                        'error' => 'Container assign to other order'
                    ]);
                }
            }

            $this->taskRepository->setTaskToPickedStatus($request->query->get('task-id'));

            $task = $this->taskRepository->find($request->query->get('task-id'));

            $taskDto = new \Craft\Dto\Task([
                'id' => $task->getId(),
                'user-id' => $task->getUserId(),
                'username' => $task->getUsername(),
                'type' => $task->getType(),
                'order-id' => $task->getOrderId(),
                'product-id' => $task->getProductId(),
                'location-id' => $task->getLocationId(),
                'status' => $task->getStatus(),
                'transit-container-id' => $containers['data'][0]['id'],
            ]);

            $taskPickedEvent = new TaskPickedEvent($taskDto);

            $this->eventDispatcher->dispatch($taskPickedEvent, PublishableEvent::EVENT_NAME);

            return $this->redirectToRoute('scan-location');
        }

        return $this->render('pda/scan-container.html.twig', [
            'form' => $form->createView(),
            'title' => self::TITLE
        ]);
    }

    private function getTaskProduct(Task $task)
    {
        $productResponse = $this->productClient->show($task->getProductId());

        if ($productResponse->getStatusCode() != Response::HTTP_OK) {
            return $this->redirectToRoute('scan-location');
        }

        return json_decode($productResponse->getBody()->getContents(), true);
    }

    private function doesLocationExists(string $barcode): bool
    {
        $locationResponse = $this->stockClient->getAllLocations(1, $barcode);

        if ($locationResponse->getStatusCode() != Response::HTTP_OK) {
            return false;
        }

        $locations = json_decode($locationResponse->getBody()->getContents(), true);

        if (!isset($locations['data'][0])) {
            return false;
        }

        return true;
    }

    private function doesProductExists(string $barcode): bool
    {
        $response = $this->productClient->getProductByBarcode($barcode);

        if ($response->getStatusCode() != Response::HTTP_OK) {
            return false;
        }

        $products = json_decode($response->getBody()->getContents(), true);

        if (!isset($products[0])) {
            return false;
        }

        return true;
    }
}

