<?php

namespace App\Repository;

use App\Entity\Task;
use Craft\Dto\Order\Order as OrderDto;
use Craft\Dto\Order\OrderLine;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function generatePickingTasksForOrder(OrderDto $order): array
    {
        $this->_em->beginTransaction();

        $tasks = [];

        try {
            /** @var OrderLine $orderLine */
            foreach ($order->products as $orderLine) {
                for ($i = 0; $i < $orderLine->quantity; $i++) {
                    $task = new Task();
                    $task->setUserId(null)
                        ->setUsername(null)
                        ->setOrderId($order->id)
                        ->setType(Task::TYPE_PICKING)
                        ->setProductId($orderLine->productId)
                        ->setLocationId($orderLine->reservedLocationId)
                        ->setStatus(Task::STATUS_UNASSIGNED)
                        ->setUpdatedAt(new DateTime())
                        ->setCreatedAt(new DateTime());

                    $this->_em->persist($task);
                    $tasks[$orderLine->id][] = $task;
                }
            }

            $this->_em->flush();
            $this->_em->commit();
        } catch (Exception $exception) {
            $this->_em->rollback();
            throw $exception;
        }

        return $tasks;
    }

    public function findInProgressTasksByLocationIds(array $ids, string $type = Task::TYPE_PICKING)
    {
        return $this->createQueryBuilder('t')
            ->where('t.type = :type')->setParameter('type', $type)
            ->andWhere('t.locationId in (:locations)')->setParameter('locations', $ids)
            ->andWhere('t.status != :status')->setParameter('status', Task::STATUS_FINISHED)
            ->getQuery()
            ->getResult();
    }

    public function generateReplenishmentTask(int $receptionId, int $productId, int $locationId): Task
    {
        $task = new Task();
        $task->setUserId(null)
            ->setUsername(null)
            ->setReceptionId($receptionId)
            ->setType(Task::TYPE_REPLENISHMENT)
            ->setProductId($productId)
            ->setLocationId($locationId)
            ->setStatus(Task::STATUS_UNASSIGNED)
            ->setUpdatedAt(new DateTime())
            ->setCreatedAt(new DateTime());

        $this->_em->persist($task);

        return $task;
    }

    public function findUserInProgressTask(string $userId, string $type = Task::TYPE_PICKING)
    {
        return $this->createQueryBuilder('t')
            ->where('t.userId = :userId')
            ->andWhere('t.type = :type')
            ->andWhere('t.status != :statusPicked and t.status != :statusFinished and t.status != :statusUnassigned')
            ->setParameters([
                'statusPicked' => Task::STATUS_PICKED,
                'statusFinished' => Task::STATUS_FINISHED,
                'statusUnassigned' => Task::STATUS_UNASSIGNED,
                'type' => $type,
                'userId' => $userId
            ])
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    public function findUnassignedTask(string $type = Task::TYPE_PICKING)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.type = :type')
            ->andWhere('t.status = :status')
            ->setParameters([
                'status' => Task::STATUS_UNASSIGNED,
                'type' => $type,
            ])
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    public function assignAllTasksForOrder(string $userId, int $orderId)
    {
        return $this->createQueryBuilder('t')->update()
            ->set('t.status', ':assignStatus')
            ->set('t.userId', ':userId')
            ->andWhere('t.orderId = :orderId')
            ->andWhere('t.status not in(:pickedStatus, :finishedStatus, :inProgressStatus)')
            ->setParameters([
                'orderId' => $orderId,
                'pickedStatus' => Task::STATUS_PICKED,
                'finishedStatus' => Task::STATUS_FINISHED,
                'inProgressStatus' => Task::STATUS_IN_PROGRESS,
                'assignStatus' => Task::STATUS_ASSIGNED,
                'userId' => $userId
            ])
            ->getQuery()
            ->execute();
    }

    public function beginTransaction()
    {
        $this->_em->beginTransaction();
    }

    public function rollback()
    {
        $this->_em->rollback();
    }

    public function commit()
    {
        $this->_em->flush();
        $this->_em->commit();
    }

    public function setTaskToPickedStatus($taskId): void
    {
        $this->createQueryBuilder('t')->update()
            ->set('t.status', ':picked')->setParameter('picked', Task::STATUS_PICKED)
            ->where('t.id = :id')->setParameter('id', $taskId)
            ->getQuery()
            ->execute();
    }
}
