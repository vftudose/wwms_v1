How to run?

* Step 1: if you don't have docker installed, please go to web and install it
* Step 2: After docker install is complete go to wwms folder
* Step 3: run script dependencies-install.sh
* Step 3: run command docker-compose -f docker-compose.yml -f docker-compose-workers.yml build (this will build the images for our applications)
* Step 4: run command docker-compose -f docker-compose.yml -f docker-compose-workers.yml up (this will run the applications)
* Step 5: open docker-compose.yml to see the services mapped ports

Project structure:
* .docker
    * frontend -> dockerfile and conf for frontend service
    * products -> dockerfile and conf for products service
    * data -> here reside the microservices db data
    * ors -> dockerfile and conf for order service aka ors
    * rabbitmq -> dockerfile and conf for rabitmq service
    * scripts -> util bash scripts
    * stock -> dockerfile and conf for stock service
    * tasks -> dockerfile and conf for tasks service
    * workers -> dockerfile for workers
* frontend -> codebase for frontend microservice
* products -> codebase for products microservice
* api -> codebase for api
* stock -> codebase for stock
* tasks -> codebase for tasks

