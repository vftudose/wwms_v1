#!/bin/bash

status=$(nc -z rabbitmq 5672; echo $?)

while [[ ${status} != 0 ]]
do
  echo 'Waiting for rabbitmq to initialize'
  sleep 1s
  status=$(nc -z rabbitmq 5672; echo $?)
done

echo 'stock consumer started'

php /var/www/site/bin/console rabbitmq:consumer events

exec "$@"