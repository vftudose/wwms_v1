#!/bin/bash

status=$(nc -z stock_db 3306; echo $?)

while [[ ${status} != 0 ]]
do
  echo 'Waiting for stock database to initialize'
  sleep 1s
  status=$(nc -z stock_db 3306; echo $?)
done

php /var/www/site/bin/console doctrine:schema:update --force

php /var/www/site/bin/console doctrine:migrations:migrate --no-interaction

exec "$@"
