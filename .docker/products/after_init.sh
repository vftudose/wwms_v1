#!/bin/bash


status=$(nc -z products_db 3306; echo $?)

while [[ ${status} != 0 ]]
do
  echo 'Waiting for products database to initialize'
  sleep 1s
  status=$(nc -z products_db 3306; echo $?)
done

php /var/www/site/bin/console doctrine:schema:update --force

exec "$@"
