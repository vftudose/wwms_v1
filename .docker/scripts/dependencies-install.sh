#!/usr/bin/env bash

cd "$PWD"/frontend && npm install && npm install react-scripts@3.0.1 -g &&

cd "$PWD"/../products/ && composer install --ignore-platform-reqs &&

cd "$PWD"/../ors/ && composer install --ignore-platform-reqs &&

cd "$PWD"/../api/ && composer install --ignore-platform-reqs &&

cd "$PWD"/../stock/ && composer install --ignore-platform-reqs
