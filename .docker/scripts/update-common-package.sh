#!/usr/bin/env bash

cd "$PWD"/products && composer update craftit/common --ignore-platform-reqs &&

cd "$PWD"/../ors && composer update craftit/common --ignore-platform-reqs &&

cd "$PWD"/../api && composer update craftit/common --ignore-platform-reqs &&

cd "$PWD"/../stock && composer update craftit/common --ignore-platform-reqs &&

cd "$PWD"/../tasks && composer update craftit/common --ignore-platform-reqs